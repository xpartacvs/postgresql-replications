# Scaling PostgreSQL - WAL Archiving dengan `pg_receivewal`

[[_TOC_]]

WAL files kita butuhkan ketika kita melakukan operasi PITR _(Point-in-Time Recovery)_. Oleh karena itu WAL files harus kita arsipkan. Untuk archiving WAL pada PG12 ada dua cara yaitu dengan `archive_command` (cara lama) atau dengan `pg_receivewal`. WAL archiving dengan `pg_receivewal` menjamin konsistensi pengarsipan WAL files lebih baik dibandingkan `archive_command`. Tutorial ini membahas langkah-langkah penerapan WAL archiving dengan `pg_receivewal`.

> :warning: **PERINGATAN**
>
> Untuk bisa sukses menerapkan tutorial ini pastikan kita sudah mengikuti keseluruhan [langkah-langkah persiapan dasar](/prerequisite/01_prerequisite.md) terlebih dahulu.

## Buat replication slot untuk `pg_receivewal`

Pada PG12 terdapat fitur bernama `replication slot`. Fitur inilah yang memastikan konsistensi archiving WAL files. Selain itu replication slot juga merupakan syarat mutlak bagi `pg_receivewal` untuk bisa berjalan. Sekarang kita akan akomodir kebutuhan tersebut dengan membuatnya di MDE dan kita sepakati nama replication slot yang akan kita buat adalah `receivewal` (gunakan user `postgres`):

```bash
psql -c "SELECT pg_create_physical_replication_slot('receivewal');"
```

## Buat direktori WAL archives

Pada tutorial ini, kita akan lakukan WAL archiving untuk PG yang berada di MDE. WAL files akan kita tarik dari MDE via `pg_receivewal` yang berjalan di RDE dan disimpan dalam direktori yang kita tentukan (di RDE).

Seperti yang sudah kita bahas diatas bahwa WAL files nantinya dibutuhkan ketika kita melakukan prosedur PITR. PITR yang dilakukan di MDE membutuhkan akses ke WAL files yang tersimpan di RDE. Oleh karena itu kita akan memanfaatkan shared directory NFS untuk menyimpan WAL files agar MDE dapat mengakses arsip-arsip WAL files dengan mudah nantinya (lakukan di RDE, gunakan user `postgres`):

```bash
mkdir -vp /share/pg/wals
```

### Buat symlink direktori WAL archives

`pg_receivewal` nantinya akan berjalan sebagai background process dengan bantuan supervisor. Saat menjalankan `pg_receivewal`, supervisor akan menggunakan user `postgres`. Untuk menghidari error yang disebabkan oleh permission filesystem, kita upayakan agar user `postgres` hanya bekerja didalam scope directory yang dimilikinya yaitu `/var/lib/postgresql`.

Diatas kita membuat direktori penampung WAL archives dilokasi berbeda. Agar direktori `/var/lib/postgresql` terhubung dengan direktori diatas, kita pelu membuat symlink didalam `/var/lib/postgresql` (lakukan di RDE, gunakan user `postgres`):

```bash
# Buat directory penampung symlinks
mkdir -vp ~/symlinks

# Buat symlnk untuk directory NFS penampung WAL files
ln -nsf /share/pg/wals ~/symlinks/wals
```

## Setup dan jalankan `pg_receivewal` dengan Supervisor

Untuk membuat `pg_receivewal` berjalan sebagai background process, kita perlu mendaftarkannya kedalam list supervisor:

### Konfigurasi supervisor untuk `pg_receivewal` di RDE

Buat sebuah file bernama `receivewal.conf` didalam `/etc/supervisor/conf.d` (misal `vim /etc/supervisor/conf.d/receivewal.conf`), isikan script dibawah ini (lakukan di RDE, jangan lupa simpan setelahnya):

```toml
[program:receivewal]
user                    = postgres
directory               = /var/lib/postgresql
command                 = pg_receivewal -D /var/lib/postgresql/symlinks/wals -S receivewal -h 10.10.10.10 -U replicator -w -v
stdout_logfile          = /var/log/supervisor/receivewal-out.log
stdout_logfile_maxbytes = 10MB
stdout_logfile_backups  = 10
stderr_logfile          = /var/log/supervisor/receivewal-err.log
stderr_logfile_maxbytes = 10MB
stderr_logfile_backups  = 10
```

### Reload dan jalankan `pg_receivewal` via supervisor

Perintahkan agar supervisor menjalankan process `pg_receivewal` sesuai konfigurasi yang sudah dibuat diatas melalui beberapa langkah berikut ini (lakukan di RDE):

```bash
# Baca ulang file-file konfigurasi yang tersedia
sudo supervisorctl reread

# Tambahkan process receivewal pada list supervisor
sudo supervisorctl add receivewal

# (optional) Periksa status process-process supervisor
# sudo supervisorctl status all
```

### Perika keberhasilan WAL archiving di RDE

Masuk ke direktori archiving yang kita tunjuk untuk menampung WAL files, kemudian periksa isi direktori. Jika direktori terisi dengan file-file maka kita telah sukses menjalankan proses WAL archiving dengan `pg_receivewal` (gunakan user `postgres`):

```bash
# Masuk ke direktori archive yang kita tunjuk
cd ~/symlinks/wals

# Periksa isi direktori
ls -la
```

## Membuat Base Backup

Sampai disini konteks WAL archiving menggunakan `pg_receivewal` sebenarnya sudah selesai. Hanya saja untuk melakukan posedur PITR, kita membutuhkan persiapan lain yaitu basebackup. Basebackup adalah proses backup menyeluruh state PG pada waktu tertentu. Backup ini nantinya akan kita kombinasikan dengan WAL files ketika perform PITR.

### Membuat direktori backup

Seperti halnya direktori WAL archives, direktori untuk file-file basebackup juga akan kita buat didalam shared directory NFS. Selain itu kita juga akan buat symlink untuk directory backup didalam home directory PG (lakukan di RDE, gunakan user `postgres`):

```bash
# Buat directory backup
mkdir -vp /share/pg/backups

# Buat symlink ke direktori backup
ln -nfs /share/pg/backups ~/symlinks/backups
```

### Perform backup dengan `pg_basebackup`

Sebelum membuat backup, pastikan mengeksekusi function`pg_switch_wal()` terlebih dahulu di MDE (gunakan user `postgres`):

```bash
psql -c "SELECT pg_switch_wal();"
```

Setelah itu barulah kita buat basebackup state PG yang ada di MDE (lakukan di RDE, gunakan user `postgres`):

```bash
pg_basebackup -h 10.10.10.10 -U replicator -w -Xn -Ft -D - | bzip2 > ~/symlinks/backups/backup-$(date +\%y\%m\%d-%H\%M\%S).tar.bz2
```
