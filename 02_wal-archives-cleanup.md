# Scaling PostgreSQL - WAL Archives Cleanup

[[_TOC_]]

Mekanisme [WAL archiving dengan `pg_receivewal`](./01_wal-archiving-dengan-pg_receivewal.md) akan menyimpan WAL files terus menerus dan berpotensi menghabiskan storage server. Pada tutorial ini kita akan membuat mekanisme untuk merotasi WAL archiives agar server tidak kehabisan storage.

> :warning: **PERINGATAN**
>
> Untuk bisa sukses menerapkan tutorial ini pastikan kita:
>
> - Sudah mengikuti keseluruhan [langkah-langkah persiapan dasar](/prerequisite/01_prerequisite.md).
> - Sudah berhasil menerapkan [WAL archiving dengan pg_receivewal](./01_wal-archiving-dengan-pg_receivewal.md).

## Wrapper Script

Secara default PG sudah menyiapkan tool yang bernama `pg_archivecleanup` untuk mengakomodir pembersihan WAL archives. Namun agar lebih mudah, kita akan memanfaatkan [BASH script dari  salah satu repository public yang ada di github](https://github.com/Napsty/scripts/blob/master/pgsql/walarchivecleanup.sh).

### Copy wrapper script

Copy keseluruhan content pada [file wrapper tersebut](https://github.com/Napsty/scripts/blob/master/pgsql/walarchivecleanup.sh), jadikan sebuah file didalam home directory PG di RDE (misal: `~/archivecleanup`, lakukan di RDE, gunakan user `postgres` saat melakukannya).

> :bulb: **Info**:
>
> - File wrapper juga bisa [ditemukan di repository ini](./tools/archivecleanup). Tapi **tidak dijamin konsistensinya terhadap update terbaru**
> - Referensi: [Automatically cleaning up archived WAL files on a PostgreSQL server](https://www.claudiokuenzler.com/blog/740/automatically-cleaning-up-archived-wal-files-on-postgresql)

Kemudian ubah permission file wrapper tersebut agar bersifat executable (gunakan user `postgres`):

```bash
# lakukan di RDE
chmod 700 ~/archivecleanup
```

### Test penggunaan wrapper script (dry-run)

Misalkan kita hendak menghapus WAL files untuk H-14 kebelakang, eksekusi perinta berikut ini di RDE (gunakan user `postgres`):

```bash
~/archivecleanup -p ~/symlinks/wals -a 14 -d -n
```

> :bulb: **Keterangan**:
>
> - Option `-n` artinya menjalankan secara simulasi saja (tidak real)
> - Option `-d` artinya printout proses yang berjalan
> - Eksekusi `~/archivecleanup` tanpa option untuk melihat petunjuk penggunaan script.

## Archive cleanup berkala

Untuk membuat mekanisme archive cleanup berkala, kita bisa memanfaatkan fitur cronjob ubuntu. Edit file cronjob dengan command `crontab -e` dan tambahkan baris berikut ini (lakukan di RDE, gunakan user `postgres`, jangan lupa simpan setelahnya):

```txt
@daily  ~/archivecleanup -p ~/symlinks/wals -a 14 >/dev/null 2>&1
```
