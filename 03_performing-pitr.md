# Scaling PostgreSQL - Performing PITR

[[_TOC_]]

Pada tutorial ini kita akan belajar untuk perform PITR dimana metode WAL archiving menggunakan `pg_receivewal`. PITR akan kita lakukan di MDE

> :warning: **PERINGATAN**
>
> Untuk bisa sukses menerapkan tutorial ini pastikan kita:
>
> - Sudah mengikuti keseluruhan [langkah-langkah persiapan dasar](/prerequisite/01_prerequisite.md).
> - Sudah berhasil menerapkan [WAL archiving dengan pg_receivewal](./01_wal-archiving-dengan-pg_receivewal.md).

## Persiapan restore data

Pada PITR kita akan me-_restore_ data PG ke titik waktu tertentu. Pada step restore tersebut PG akan mencari material (dalam hal ini adalah archives WAL files) untuk merestore data didalam direktori yang kita tentukan di option `restore_command`.

### Buat direktori penampung WAL files untuk proses restore

Disini kita akan sepakati bahwa directory yang akan kita gunakan untuk menampun WAL files (untuk kebutuhan restore) adalah `/var/lib/postgresql/restore`. Maka dari itu kita buat terlebih dahulu direktori tersebut (lakukan di MDE, gunakan user `postgres`):

```bash
mkdir -vp ~/restore
```

### Buat file konfigurasi untuk merestore WAL files pada mode recovery

Konfigurasi yang ada didalam file ini nantinya akan digunakan oleh PG untuk merestore data pada mode `recovery`.

Buat sebuah file bernama `recovery.conf` didalam `/etc/postgresql/12/main/conf.d` (lakukan di MDE, gunakan user `postgres`), kemudian isikan dengan script dibawah ini (jangan lupa simpan setelahnya):

```ini
restore_command = 'cp /var/lib/postgresql/restore/%f %p'
```

### Restart service PG

Agar konfigurasi yang sudah kita buat dapa segera di terapkan oleh PG, restart cluster PG (lakukan di MDE):

```bash
# Restart cluster main di MDE
sudo systemctl restart postgresql@12-main
```

## Simulasi insiden

Berikut ini adalah skenario yang akan kita jalankan untuk mensimulasikan insiden kehilangan data:

1. Penambahan data awal
2. Penambahan data berikutnya (5 menit setelah no.1)
3. Hapus sebagian data (5 menit setelah no.2)
4. Eksekusi PITR (kembalikan data pada keadaan no.2)

Untuk memudahkan kita pada step penambahan data, kita akan buat sebuah BASH script yang nanti akan kita eksekusi. Misal `vim ~/populate` (lakukan di MDE, gunakan user `postgres`, jangan lupa simpan setelahnya):

```bash
#!/usr/bin/bash

# Buat database db_dummy
psql -c "CREATE DATABASE db_dummy;"

# Buat tabel sample
psql db_dummy -c "CREATE TABLE sample (id serial not null, amount int8 not null, time_create timestamp not null default current_timestamp);"


# Isi data dummy awal
for n in {1..5}; do
        psql db_dummy -c "INSERT INTO sample (amount) VALUES ($RANDOM);"
        sleep 1
done

# Tampilkan data exisiting (optonal)
psql db_dummy -c "SELECT * FROM sample;"
```

> Pada script diatas kita membuat database bernama `db_dummy`, tabel bernama `sample`, mengsikan 5 data kedalam tabel `sample` (degan jeda 1 detik setiap data) dan terakhir kita menampilkan isi table `sample`.

Kemudian ubah permission agar bash script tersebut bersifat `executable`:

```bash
chmod 700 ~/populate
```

### Penambahan data

Eksekusi bash script yang sudah kita buat diatas (lakukan di MDE, gunakan user `postgres`):

```bash
./populate
```

Catat timestamp data terakhir yang tambil, kemudian tunggu 5 menit. Ulangi lagi eksekusi script, kembali catat timestamp data terakhir yang tambil, kemudian tunggu 5 menit lagi.

### Simulasikan insiden kehilangan data

Eksekusi perinta berikut ini untuk mensimulasikan insiden kehilangan data dan catat waktu insidennya (lakukan di MDE, gunakan user `postgres`):

```bash
psql db_dummy -c "DELETE FROM sample WHERE id IN (SELECT id FROM sample LIMIT 5);"
```

## Performing PITR

Pada step ini kita akan berusaha mengembalikan data ke keadaan no.2 di skenario simlulasi kehilangan data dengan bekal catatan waktu pada step no.2.

> :warning: **Penting**
>
>Disini kita sepakati asumsi bahwa catatan waktu step no.2 pada simulasi kehilangan data adalah `2022-05-13 13:00:00.000`.

### Stop service PG

Langkan awal untuk PITR adalah dengan menghentikan service PG:

```bash
# lakukan pada node dimana PITR hendak diterapkan (MDE)
sudo systemctl stop postgresql@12-main
```

### Pastikan direktori restore dalam keadaan kosong

Periksa apakah ada file dialam direktori restore yang sudah kita siapkan pada [step diatas](#buat-direktori-penampung-wal-files-untuk-proses-restore). Jika ada, kosongkan dengan perintah `rm -vRf ~/restore/*` (lakukan di MDE, gunakan user `postgres`).

### Hapus seluruh ini direktori data utama PG

Yap seperti judul header di section ini, hapus seluruh ini direktori data utama PG (lakukan di MDE, gunakan user `postgres`):

```bash
rm -vRf ~/12/main/* 
```

> :warning: **Perhatian**
>
> - Untuk berjaga-jaga, disarankan untuk backup terlebih dahulu.
> - Nantinya direktori ini akan diisi dengan content dari file base backup.

### Isi kembali direktori data utama PG

Eksekusi perintah berikut ini untuk mengisi kembali direktori data utama PG dengan content base backup (lakukan di MDE, gunakan user `postgres`):

```bash
tar -vxj -f /mnt/nfs/share/pg/backups/backup-yymmdd-hhmmss.tar.bz2 -C ~/12/main
```

### Isi direktori restore dengan WAL archives

Eksekusi perintah berikut ini untuk mengisi direktori restore dengan WAL files yang kita terapkan mekanisme archivingnya pada tutorial [WAL archiving](./01_wal-archiving-dengan-pg_receivewal.md) (lakukan di MDE, gunakan user `postgres`):

```bash
cp /mnt/nfs/share/pg/wals/* ~/restore/
```

#### Hilangkan suffix `.partial`

Periksa direktori `~/restore` apakah ada file-file yang ber-_suffix_ `.partial`. Jika ada, hilangkan _(rename)_ suffix `.partial` pada masing-masing file tersebut.

### Konfigurasi titik waktu recovery

Masuk ke `/etc/postgresql/12/main/conf.d`, edit file `recovery.conf` (yang sudah kita buat [sebelumnya](#buat-file-konfigurasi-untuk-merestore-wal-files-pada-mode-recovery)), tambahkan baris berikut ini (lakukan di MDE, gunakan user `postgres`, jangan lupa simpan setelahnya):

```ini
recovery_target_time = '2022-05-13 13:00:00.000'
```

> :warning: Isi value sesuai [catatan waktu](#performing-pitr) yang kita simpan untuk skenario step no.2

### Konfigurasi recovery mode

PITR hanya bisa dilakukan pada mode recovery. Untuk menginstruksikan agar PG berjalan dalam mode recovery, buat sebuah file kosong bernama `recovery.signal` didalam direktori utara cluster PG (lakukan di MDE, gunakan user `postgres`):

```bash
touch ~/12/main/recovery.signal
```

> :bulb: Nantinya file `recovery.signal` akan terhapus secara otomatis setelah PG keluar dari mode recovery.

### Start service PG

Nyalakan kembali service PG. Jika berhasil, PG akan berjalan dalam mode recovery:

```bash
# lakukan pada node dimana PITR hendak diterapkan (MDE)
sudo systemctl start postgresql@12-main
```

> :bulb: Sebelum menyalakan kembali service PG, disarankan untuk memonitor log PG (dengan session terminal yang berbeda) untuk melihat aktifitas service (misal: `tail -f /var/log/postgresql/postgresql-12-main.log`).

### Resume ke titik waktu recovery

Ketika berjalan pada mode recovery, PG tidak akan mematuhi operasi query selain operasi read. Selain itu, pada state ini PG belum berada pada keadaan di titik waktu yang kita tentukan di option `recovery_target_time`. Untuk membuatnya menuju ke titik waktu tersebut (sekaligus keluar dari mode recovery), eksekusi perintah berikut ini (lakukan di MDE, gunakan user `postgres`):

```bash
psql -c "SELECT pg_wal_replay_resume();"
```

### Recover mekanisme WAL archiving

Setelah PITR sukses, mekanisme WAL archiving kita jadi rusak karena replication slot yang digunakan ikut terhapus pada [step penghapusan isi direktori cluster utama](#hapus-seluruh-ini-direktori-data-utama-pg). Oleh karena itu kita perlu [membuat kembali replication slot tersebut](./01_wal-archiving-dengan-pg_receivewal.md#buat-replication-slot-untuk-pgreceivewal).

### Recover mekanisme streaming replication

> :warning: **PERINGATAN**:
>
> Section ini hanya berlaku jika kita sudah sukses menerapkan [streaming replication](./04_streaming-replication.md).

Seperti halnya mekanisme WAL archiving, mekanisme streaming replication juga rusak setelah PITR sukses karena replication slot yang digunakan ikut terhapus pada [step penghapusan isi direktori cluster utama](#hapus-seluruh-ini-direktori-data-utama-pg). Bedanya (dan sayangnya) kita harus mengulangi [langkah pembuatan streaming replication](./04_streaming-replication.md) dari nol.
