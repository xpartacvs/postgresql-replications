# PostgreSQL 12  - Streaming Replication

[[_TOC_]]

Pada tutorial ini kita akan membangun streaming replication sebagai solusi failover. Streaming replication memungkinkan RDE mengambil peran sebagai database utama ketika MDE mengalami kegagalain _(failure)_.

> :warning: **PERINGATAN**
>
> Untuk bisa sukses menerapkan tutorial ini pastikan kita sudah mengikuti keseluruhan [langkah-langkah persiapan dasar](/prerequisite/01_prerequisite.md).

## Membuat Replication Slot

Streaming replication yang akan kita buat memanfaatkan `replication slot`. Dan karena replication slot menggunakan stream protocol, ia menjamin konsistensi replikasi antara MDE & RDE. Sekarang mari kita buat replication slot (lakukan di MDE, gunakan user `postgres`):

```bash
# Buat replication slot
psql -c "SELECT * FROM pg_create_physical_replication_slot('stream');"

# Lihat status replication slot (optional)
# psql -c "SELECT * FROM pg_replication_slots;"
```

> :warning: Disini kita sepakati bahwa nama replication slot yang akan kita gunakan adalah `stream`.

## Setup Streaming Replication

Streaming replication akan kita konfigurasi di RDE. Berikut ini langka-langkah yang perlu dilakukan:

### Stop service PG

Akan ada step dimana kita harus mengosongkan direktori cluster di RDE. Supaya aman, kita stop dulu service PG di RDE:

```bash
# Lakukan di RDE
sudo systemctl stop postgresql@12-replica
```

### Kosongkan direktori cluster PG

Selanjutnya kita harus mengosongkan direktori cluster utama PG di RDE (gunakan user `postgres`):

```bash
rm -vRf ~/12/replica/*
```

### Tarik DB dari MDE dengan `pg_basebackup`

Sekarang kita jalankan prosedur basebackup untuk mendownload keseluruhan data yang ada di MDE. Jalankan command berikut ini di RDE (pastikan menggunakan user `postgres`):

```bash
pg_basebackup -h 10.10.10.10 -U replicator -D ~/12/replica -S stream -w -P -R -Xs
```

| **Option** | **Keterangan**                                                                                                                   |
| :---       | :---                                                                                                                             |
| `-h`       | Host atau alamat IP dari server database utama yang hendak di-_copy_ datanya.                                                    |
| `-U`       | Username yang digunakan untuk connect ke MDE                                                                                     |
| `-D`       | Path ke direktori tujuan dimana data yang di-_copy_ dari database utama hendak disimpan                                          |
| `-S`       | Nama replication slot yang hendak digunakan. Lihat [step membuat replication slot](#membuat-replication-slot).                   |
| `-w`       | Opsi agar prompt permintaan password tidak dimunculkan (karena user `replicator` yang kita buat tidak menggunakan password)      |
| `-P`       | Opsi agar menampilkan indikator progress (optional)                                                                              |
| `-R`       | Opsi yang menginstruksikan PG untuk membuat beberapa file penting untuk menjalankan proses replikasi _(akan kita bahas dibawah)_ |
| `-Xs`      | Opsi yang menginstruksikan PG untuk mengambil juga WAL files dengan metode `stream`                                              |

Apa yang terjadi ketika command `pb_basebackup` diatas dijalankan? Berikut ini sekilas aktifitasnya:

1. PG di RDE akan berusaha terhubung dengan PG di MDE menggunakan credential yang kita input pada option `-h, -U,` dan `-w`.
2. Setelah terhubung, data di MDE keseluruhannya akan di-_copy_ kedalam directori di RDE yang path-nya kita sebutkan di option `-D`. Untuk memastikannya kita bisa periksa kembali isi dari direktori data RDE yang tadi sudah kita kosongkan (`/var/lib/postgresql/12/replica`). Idealnya direktori tersebut akan terisi oleh data yang di-_copy_ dari MDE.
3. Selain itu WAL files juga akan diambil dengan metode `stream`. Perilaku ini diakibatkan oleh option `-Xs` yang kita gunakan.
4. PG juga akan menulis beberapa file penting untuk menjalankan proses replication. File-file ini antara lain adalah `postgresql.auto.conf` dan `standby.signal` yang keduanya tersimpan berdampingan dengan data cluster di dalam `/var/lib/postgresql/12/replica`. File `postgresql.auto.conf` adalah file yang berisi pengaturan yang nantinya akan digunakan oleh PG di RDE untuk bisa terhubung dengan MDE. File ini tidak disarankan untuk diubal-ubah secara manual. Sedangkan file `standby.signal` adalah sebuah file kosong yang nantinya (saat PG di RDE diaktifkan kembali) digunakan oleh PG sebagai penanda bahwa cluster harus berjalan dalam mode standby, dan pada mode standy operasi yang diizinkan hanya `SELECT` (read-only). File ini nantinya akan dihapus secara otomatis oleh PG di RDE setelah digunakan.

### Nyalakan kembali service PG

Langkah terakhir ialah dengan menyalakan kembali service PG di RDE:

```bash
# Lakukan di RDE
sudo systemctl start postgresql@12-replica
```

## Pengujian Streaming Replication

Setelah tahap pembuatan streaming replication kita lakukan, ada baiknya kita melakukan pengujian terhadap hasil streaming replication.

### Periksa status replikasi dengan command `pg_lsclusters`

Jalankan command `sudo pg_lsclusters` di MDE dan RDE, kemudian periksa value yang tertulis pada kolom `Status`. Jika streaming replication berjalan lancar maka value yang tertulis untuk MDE idelanya adalah`online` sedangkan value yang tertulis di RDE idealnya adalah `online,recovery`.

### Periksa status replikasi dengan query

Jika RDE berjalan mulus dalam standby mode setelah diaktifkan kembali, maka idealnya RDE akan terkoneksi dengan MDE. Untuk membuktikannya kita bisa menjalankan command berikut ini di MDE (pastikan menggunakan user `postgres`):

```bash
psql -c "SELECT client_addr, state FROM pg_stat_replication;"

# The ideal output should be like:
#
#  client_addr    |  state
# ----------------+-----------
# your_replica_IP | streaming
```

Jika streaming replication berjalan lancar maka command diatas idealnya akan menampilkan table dengan 2 kolom dimana kolom pertama (`client_addr`) berisi alamat IP dari RDE dan kolom kedua (`state`) berisi status `streaming`.

### Validasi replikasi dengan operasi tulis

Selain dengan query ke tabel `pg_stat_replication`, kita juga sebaiknya melakukan pengujian dengan operasi tulis.

#### Operasi tulis dari MDE

Idealnya untuk setiap operasi tulis yang terjadi di MDE, RDE akan mereplikanya 100%. Untuk membuktikannya kita bisa uji dengan membuat sebuat database baru di MDE (pastikan menggunakan user `postgres`):

```bash
# Lakukan di MDE
psql -c "CREATE DATABASE db_test;"
```

Command diatas akan mengakibatkan PG di MDE menciptakan sebuah database baru bernama `db_test` yang idealnya juga akan tercipta di RDE. Oleh karena itu kita akan periksa kebenarannya di RDE (pastikan menggunakan user `postgres`):

```bash
# Lakukan di RDE
psql -c "\l"

# The output shoud be like:
#
#                              List of databases
#    Name    |  Owner   | Encoding | Collate |  Ctype  |   Access privileges   
# -----------+----------+----------+---------+---------+-----------------------
#  db_test   | postgres | UTF8     | C.UTF-8 | C.UTF-8 | 
#  postgres  | postgres | UTF8     | C.UTF-8 | C.UTF-8 | 
#  template0 | postgres | UTF8     | C.UTF-8 | C.UTF-8 | =c/postgres          +
#            |          |          |         |         | postgres=CTc/postgres
#  template1 | postgres | UTF8     | C.UTF-8 | C.UTF-8 | =c/postgres          +
#            |          |          |         |         | postgres=CTc/postgres
# (4 rows)
```

Jika pada kolom pertama dari tabel yang muncul terdapat `db_test`, maka bisa dikatakan streaming replication berjalan sebagaimana mestinya.

#### Operasi tulis di RDE

Telah kita bahas sebelumnya bahwa ketika suatu cluster PG berjalan sebagai replika (mode standby) maka operasi yang diizinkan hanyalah operasi baca (read-only). Untuk membuktikannya kita bisa melakukan pengujian dengan mencoba menjalankan operasi tulis pada RDE (pastikan menggunakan user `postgres`):

```bash
# Lakukan di RDE
psql -c "DROP DATABASE db_test;"

# The output shoud be:
#
# ERROR:  cannot execute DROP DATABASE in a read-only transaction
```

Jika command diatas dieksekusi di RDE, idealnya PG akan menampilkan pesan error `ERROR:  cannot execute DROP DATABASE in a read-only transaction`. Ini artinya operasi tulis tidak diizinkan dan bisa dikatakan streaming replication berjalan sebagaimana mestinya.

### Simuasi failure pada MDE dan solusi failover

Pengujian terakhir yang dapat kita lakukan adalah dengan meng-simulasikan kegagalan pada MDE dimana pada situasi ini RDE dapat mengambil peran sebagai node database utama menggantikan MDE.

#### Simulasikan kegagalan MDE

Cara paling sederhana untuk mensimulasikan kegagalan MDE adalah dengan menonaktifkan service PG-nya:

```bash
# Lakukan di MDE
sudo systemctl stop postgresql@12-main
```

#### Promosikan RDE sebagai node DB utama pengganti MDE

Telah kita singgung sebelumnya bahwa salah satu benefit dari streaming replication adalah sebagai solusi failover ketika node database utama (MDE) mengalami kegagalan. Pada situasi tersebut RDE dapat mengambil peran menggantikan MDE sebagai database utama. Untuk melakukannya kita akan menggunakan command `pg_ctlcluster` di RDE:

```bash
# Lakukan di RDE
pg_ctlcluster 12 replica promote
```

> :bulb:**Tips**:
>
> - Pastikan menggunakan user `postgres`
> - Pantau keberhasilannya pada log RDE dengan mengeksekusi command `tail -n 100 /var/log/postgresql/postgresql-12-replica.log`.

#### Uji operasi tulis pada RDE setelah dipromosikan

Setelah RDE dipromosikan menjadi node DB utama pengganti MDE, idealnya RDE tidak lagi hanya menerima operasi baca melainkan juga operasi tulis. Untuk memastikannya lakukan operasi tulis pada RDE seperti contoh berikut ini (pastikan menggunakan user `postgres`):

```bash
# Lakukan di RDE
psql -c "DROP DATABASE db_test;"
```

Jika output dari command diatas adalah `DROP DATABASE`, maka operasi tulis pada RDE telah diizinkan dan ini artinya RDE telah sukses menggantikan MDE sebagai DB utama.
