# Seri Scaling PostgreSQL

Harap ikuti tutorial secara berurutan:

1. [WAL archiving dengan `pg_receivewal`](./01_wal-archiving-dengan-pg_receivewal.md)
2. [WAL archives cleanup](./02_wal-archives-cleanup.md)
3. [Performing PITR](03_performing-pitr.md)
4. [Streaming Replication](./04_streaming-replication.md)
