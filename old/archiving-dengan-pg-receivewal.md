# Archiving dengan `pg_receivewal`

[[_TOC_]]

WAL archiving dengan `pg_receivewal` menjamin konsistensi pengasipan WAL files lebih baik dibandingkan `archive_command`.

## Tahap Persiapan

### Requirement Dasar

- 2 server ubuntu 20.04 dalam satu jaringan yang sama
- Masing-masing server sudah terinstall PostgreSQL 12
- Server Utama (MDE) kita asumsikan memiliki IP **10.10.1.10**
- Server Replica (RDE) kita asumsikan memiliki IP **10.10.1.20**
- [(Optional) Untuk menghindari kerancuan, rename cluster PG di RDE](https://gitlab.com/xpartacvs/postgresql-replications/-/blob/main/streaming-replication.md#user-content-mengubah-nama-cluster)

### Requirement Tambahan

Kita akan menjalankan `pg_receivewal` dari RDE untuk menarik WAL files yang ada di MDE dan menyimpannya di salah satu direktori yang kita tentukan di RDE. WAL files yang kira simpan di RDE nantinya akan kita pergunakan kembali saat melakukan PITR di MDE.

Untuk memudahkan proses transmisi file antar file, kita membutuhkan suatu media transmisi antar node. Oleh karena itu kita akan menggunakan NFS (Network File System).

#### Install dan konfigurasi NFS Server pada RDE

Sebelum menginstall server NFS di RDE, kita akan membuat sebuat direktori yang nantinya akan ditunjuk oleh NFS sebagai shared direktori antara MDE dan RDE (network direktory):

```bash
# Buat sebuah direktori baru
sudo mkdir -vp /sharedir

# Ubah permission direktori agar bisa diakses semua pihak
sudo chmod -vRf 666 /sharedir

# Ganti ownershio direktori menjadi standard NFS
sudo chown -vRf nobody:nogroup /sharedir
```

Sekarang kita bisa install server NFS:

```bash
# Update APT
sudo apt update

# Install server NFS
sudo apt -y install nfs-kernel-server
```

Setelah berhasil terinstall, kita konfigurasikan NFS agar menujuk direktori yang sudah kita siapkan diatas sebagai shared direktory antar node.

Buka dan edit file `/etc/exports` (misal:`sudo vim /etc/exports`) dan tambahkan baris berikut ini (jangan lupa simpan setelahnya):

```txt
/sharedir   10.10.1.1/24(rw,sync,no_root_squash,no_subtree_check)
```

Setelah itu restart server NFS dan propagate agar konfigurasi menyebar ke jaringan:

```bash
# Restart server NFS
sudo systemctl restart nfs-kernel-server

# Propagate konfigurasi ke network
sudo exportfs -va
```

#### Install dan konfigurasi NFS client di MDE

Sama seperti langkah pendahuluan sebelum menginstall NFS Server di RDE, kita juga perlu membuat direktori khusus di MDE yag nantinya akan kita _mount_ sebagai network direktori NFS:

```bash
# Buat sebuah direktori baru
sudo mkdir -vp /mnt/nfs/sharedir

# Ubah permission direktori agar bisa diakses semua pihak
sudo chmod -vRf 666 /mnt/nfs/sharedir

# Ganti ownershio direktori menjadi standard NFS
sudo chown -vRf nobody:nogroup /mnt/nfs/sharedir
```

Setelah itu kita lanjutkan dengan menginstall NFS Client:

```bash
# Update APT
sudo apt update

# Install client NFS
sudo apt -y install nfs-common
```

Setelah ter-_install_, kita lakukan konfigurasi agar direktori khusus yang kita buat sebelumnya di-_mount_ sebagai network directory NFS yang terhubung dengan RDE.

Buka dan edit file `/etc/fstab` (misal:`sudo vim /etc/fstab`) dan tambahkan baris berikut ini (jangan lupa simpan setelahnya):

```txt
10.10.1.20:/sharedir    /mnt/nfs/sharedir   nfs4    auto,noatime,nolock,bg,nfsvers=4,intr,tcp,actimeo=1800  0   0
```

Kemudian restart MDE (misal: `sudo reboot`), SSH kembali ke MDE dan pastikan direktori `/mnt/nfs/sharedir` di MDE sudah ter-_mount_ dengan direktori `/sharedir` di RDE dengan command:

```bash
sudo df -h
```

#### Install supervisor di RDE

Telah kita singgung sebelumnya bahwa `pg_receivewal` akan kita jalankan di RDE. Dan secara default jika `pg_receivewal` dijalankan, maka ia akan berjalan di _foreground_. Akibatnya session terminal aktif yang kita gunakan untuk menjalankannya akan tersita. Ini tidak _best practice_.

Oleh karena itu kita butuh suatu tool yang akan membantu kita menjalankan `pg_receivewal` di background. Dan tool yang akan kita gunakan adalah `supervisor`. Sekarang mari kita install supervisor:

```bash
# Update APT
sudo apt update

# Install supervisor
sudo apt -y install supervisor
```

## Tahap Eksekusi

### Persiapkan role replication di MDE

Kita perlu membuat role pada MDE yang nantinya akan dipakai oleh RDE untuk beberpa keperluan antara lain membuat file basebackup dan menjalankan `pg_receivewal` (asumsi role bernama `replicator`, gunakan user `postgres`):

```bash
psql -c "CREATE ROLE replicator WITH REPLICATION LOGIN;"
```

### Persiapkan replication slot di MDE

`pg_receivewal` hanya bisa berjalan dengan memanfaatkan replication slot. Oleh karena itu kita buat terlebih dahulu (asumsi replicarion slot bernama `receivewal`, gunakan user `postgres`):

```bash
psql -c "SELECT pg_create_physical_replication_slot('receivewal');"
```

### Buat direktori archive di RDE

WAL files yang akan kita tarik dari MDE via `pg_receivewal` harus disipan kedalam suatu direktori yang kita tentukan, oleh karena itu mari kita buat (gunakan user `postgres`):

```bash
mkdir -vp ~/archives/12/receivewal
```

### Mem-_populate_ Data Sample di MDE

Buat sebuah database dan table untuk data dummy (gunakan user `postgres`):

```bash
# Buat database db_dummy
psql -c "CREATE DATABASE db_dummy;"

# Buat tabel sample
psql db_dummy -c "CREATE TABLE sample (id serial not null, amount int8 not null, time_create timestamp not null default current_timestamp);"
```

Buatlah bash script untuk mempopulate sample data dummy. Misal `touch /var/lib/postgresql/populate`, buka dengan text editor dan isikan scrip dibawah ini:

```bash
#!/usr/bin/bash

# Isi data dummy awal
for n in {1..10}; do
        psql db_dummy -c "INSERT INTO sample (amount) VALUES ($RANDOM);"
        sleep 1
done

# Tampilkan data exisiting (optonal)
# psql db_dummy -c "SELECT * FROM sample;"
```

Ubah mode file sehingga bersifat _executable_, eksekusi file tersebut, dan catat `timestamp` eksekusinya:

```bash
# Ubah mode script menjadi executable
chmod u+x /var/lib/postgresql/populate

# Eksekusi script dan catat timestamp eksekusinya
/var/lib/postgresql/populate
```

Misalkan timestamp eksekusinya adalah pada `2022-03-15 14:00:00.000`, maka catat timestamp ini karena akan kita pakai nantinya.

### Buat file basebackup di RDE

Untuk kebutuhan PITR di MDE nantinya, kita perlu mempersiapkan file base backup. File basebackup ini akan kita simpan dalam direktori khusus (gunakan user `postgres`):

```bash
# Jalankan perintah switch WAL (di MDE)
psql -c "SELECT pg_switch_wal();"

# Buat direktori penampung basebackup (di RDE)
mkdir -vp ~/backups

# Buat file basebackup (di RDE)
pg_basebackup -h 10.10.1.10 -U replicator -w -Ft -Xn -D - | bzip2 > ~/backups/backup-$(date +\%y\%m\%d-%H\%M\%S).tar.bz2
```

### Konfigurasi supervisor untuk `pg_receivewal` di RDE

Sekarang kita akan membuat sebuah file konfigurasi agar supervisor menjalankan `pg_receivewal` sebagai _background process_. Buat sebuah file bernama `receivewal.conf` didalam `/etc/supervisor/conf.d` (misal `vim /etc/supervisor/conf.d/receivewal.conf`), isikan script dibawah ini (jangan lupa simpan setelahnya):

```toml
[program:receivewal]
user                    = postgres
directory               = /var/lib/postgresql
command                 = pg_receivewal -D /var/lib/postgresql/archives/12/receivewal -S receivewal -h 10.10.1.10 -U replicator -w -v
stdout_logfile          = /var/log/supervisor/receivewal-out.log
stdout_logfile_maxbytes = 10MB
stdout_logfile_backups  = 10
stderr_logfile          = /var/log/supervisor/receivewal-err.log
stderr_logfile_maxbytes = 10MB
stderr_logfile_backups  = 10
```

Setelah itu perintahkan agar supervisor menjalankan process `pg_receivewal` sesuai konfigurasi yang sudah dibuat diatas melalui beberapa langkah berikut ini:

```bash
# Baca ulang file-file konfigurasi yang tersedia
sudo supervisorctl reread

# Tambahkan process receivewal pada list supervisor
sudo supervisorctl add receivewal

# (optional) Periksa status process-process supervisor
# supervisorctl status all
```

### Perika keberhasilan WAL archiving di RDE

Masuk ke direktori archiving yang kita tunjuk untuk menampung WAL files, kemudian periksa isi direktori. Jika direktori terisi dengan file-file maka kita telah sukses menjalankan proses WAL archiving dengan `pg_receivewal` (gunakan user `postgres`):

```bash
# Masuk ke direktori archive yang kita tunjuk
cd ~/archives/12/receivewal

# Periksa isi direktori
ls -la
```

## :tada: Selamat

Sampai disini kita sudah berhasil membangun solusi WAL archiving dengan memanfaatkan `pg_receivewal` pada PG. **Good Job !!!**
