# PITR ketika WAL archiving menggunakan `pg_receivewal`

[[_TOC_]]

Pada tutorial ini kita akan belajar untuk perform PITR dimana metode WAL archiving memanfaatkan `pg_receivewal`.

## Tahap Persiapan

Tutorial ini hanya akan berhasil jika kita sudah sukses [membuat solusi WAL archiving menggunakan `pg_receivewal`](https://gitlab.com/xpartacvs/postgresql-replications/-/blob/main/archiving-dengan-pg-receivewal.md)

### Buat direktori penampung WAL files untuk restore data di MDE

Direktori ini nantinya akan menampung WAL files yang tersimpan di RDE (via `pg_receivewal`) untuk kemudian digunakan sebagai material untuk merestore data (gunakan user `postgres`):

```bash
# Lakukan di MDE
mkdir -vp ~/archives/12/main
```

### Buat file konfigurasi untuk merestore WAL files pada mode recovery di MDE

Konfigurasi yang ada didalam file ini nantinya akan digunakan oleh PG untuk merestore data pada mode `recovery`.

Buat sebuah file bernama `recovery.conf` didalam `/etc/postgresql/12/main/conf.d` (gunakan user `postgres`), kemudian isikan dengan script dibawah ini (jangan lupa simpan setelahnya):

```ini
restore_command         = 'cp /var/lib/postgresql/archives/12/main/%f %p'
archive_cleanup_command = 'pg_archivecleanup /var/lib/postgresql/archives/12/main %r'
```

## Simulasikan penambahan data baru

Eksekusi scipt penambahan data yang sudah kita buat sebelumnya (`/var/lib/postgresql/populate`) beberapa kali namun beri jeda waktu berbeda setiap eksekusinya (misal per 10 menit), catat waktu eksekusinya, dan catat keadaan data pada setiap eksekusi. Misal (user: `postgres`):

```bash
# Eksekusi script peambahan data
/var/lib/postgresql/populate

# Print keadaan tabel, dan outputkan 
# berupa file dengan timestamp sebagai 
# penanda waktunya
psql db_dummy -c "SELECT * FROM sample;" > table-sample-at-$(date +\%y\%m\%d-%H\%M\%S).txt
```

## Simulasikan insiden kehilangan data

Asumsi kita kehilangan 5 data awal dan 5 data akhir (user: `postgres`):

```bash
# Simulasi kehilangan 5 data awal
psql db_dummy -c "DELETE FROM sample WHERE id IN (SELECT id FROM sample ORDER BY id LIMIT 5)";

# Simulasi kehilangan 5 data terakhir
psql db_dummy -c "DELETE FROM sample WHERE id IN (SELECT id FROM sample ORDER BY id DESC LIMIT 5)";
```

Print keadaan terakhir tabel, dan outputkan berupa file dengan timestamp sebagai penanda waktunya:

```bash
psql db_dummy -c "SELECT * FROM sample;" > table-sample-at-$(date +\%y\%m\%d-%H\%M\%S).txt
```

## PITR in Action

### Stop Cluster di MDE

Hentikan cluster terkait (gunakan user dengan privilege `sudo`):

```bash
sudo systemctl stop postgresql@12-main
```

### Selamatkan direktori `pg_wal` di MDE

Copy atau pindahkan direktori `/var/lib/postgresql/12/main/pg_wal` ke home directory (gunakan user `postgres`):

```bash
mv -v ~/12/main/pg_wal ~/
```

### Hapus seluruh data cluster dan WAL files yang ada di direktori restore di MDE

Hapus seluruh data di cluster dan directori restore (user: `postgres`):

```bash
# Masuk dan hapus semuanya data dari direktori cluster
cd ~/12/main && rm -rf *

# Masuk dan hapus semuanya data daru direktori restore
cd ~/archives/12/main && rm -rf *
```

### Kirim file backup dan WAL files dari RDE ke MDE melalui NFS

```bash
# Copy file basebackup di RDE ke netwrok directori (di RDE)
sudo cp -vRip /var/lib/postgresql/backups/backup-yymmdd-HHMMSS.tar.bz2 /sharedir/

# Copy WAL files di RDE ke netwrok directori (di RDE)
sudo cp -vRip /var/lib/postgresql/archives/12/receivewal /sharedir/
```

### Exctract file base backup

Extract file base backup yang ada di network directory NFS ke direktoi cluster, hapus direktori `pg_wal` yang ada didalamnya, dan kembalikan file `pg_wal` yang sebelumnya sudah kita selamatkan kedalam diretory cluster:

```bash
# Extract file base backup (lakukan di MDE)
sudo tar -vxj -f /mnt/nfs/sharedir/backup-yymmdd-HHMMSS.tar.bz2 -C /var/lib/postgresql/12/main

# Hapus direktorui pg_wal bawaan basebackup (lakukan di MDE)
sudo rm -rf /var/lib/postgresql/12/main/pg_wal

# Kembalikan direktori pg_wal kedalam diretory cluster (lakukan di MDE)
mv -v /var/lib/postgresql/pg_wal /var/lib/postgresql/12/main/
```

### Copy WAL files ke direktori restore

Selanjutnya kita harus memasukkan seluruh WAL files yang ada di network direktory NFS ke direktori restore dan me-_rename_ seluruh file yang ber-ekstensi `.partial` sehingga file tersebut  tidak mempunyai ekstensi:

```bash
# Copy WAL files ke direktory restore (di MDE)
sudo cp -vRip /mnt/nfs/sharedir/receivewal/* /var/lib/postgresql/archives/12/main/

# Rename file berekstensi .partial (di MDE).
mv -v /var/lib/postgresql/archives/12/main/00001.partial /var/lib/postgresql/archives/12/main/00001
```

### Tambahkan konfigurasi penentu titik restore

Edit file `/etc/postgresql/12/main/conf.d/recovery.conf` dan tambahkan baris berikut ini (jangan lupa simpan setelahnya):

```ini
# Isikan value dengan titik timestamp dimana kita menghendaki data tersebut di-restore
recovery_target_time    = '2022-04-27 06:00:00.000'
```

### Buat file untuk memicu mode recovery di MDE

Gunakan user `postgres`:

```bash
# Lakukan di MDE
touch ~/12/main/recovery.signal
```

### Nyalakan kembali cluster MDE

```bash
# Lakukan di MDE
sudo systemctl start postgresql@12-main
```

### Jalankan perintah resume

Jalankan perintah resume agak data direstore ke titik yang kita tentukan di file konfigurasi (gunakan user `postgres`):

```bash
psql -c "SELECT pg_wal_replay_resume();"
```
