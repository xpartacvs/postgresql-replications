# PostgreSQL 12 - Point-in-Time Recovery (PITR)

PITR adalah fitur PG yang memungkinkan DBA mengembalikan keadaan suatu cluster PG pada suatu titik waktu tertentu.

## Mengaktifkan Log Archiving

Buat sebuah direktori untuk log archives (user: `postgres`):

```bash
mkdir -vp /var/lib/postgresql/archives/12/main
```

Buat sebuah file baru bernama `archiving.conf` didalam `/etc/postgresql/12/main/conf.d`, isikan:

```ini
archive_mode    = on
archive_command = 'test ! -f /var/lib/postgresql/archives/12/main/%f && cp %p /var/lib/postgresql/archives/12/main/%f'
```

Restart cluster (gunakan user dengan  privilege `sudo`):

```bash
sudo systemctl restart postgresql@12-main
```

## Mem-_populate_ Data Sample

Buat sebuah database dan table untuk data dummy (gunakan user `postgres`):

```bash
# Buat database db_dummy
psql -c "CREATE DATABASE db_dummy;"

# Buat tabel sample
psql db_dummy -c "CREATE TABLE sample (id serial not null, amount int8 not null, time_create timestamp not null default current_timestamp);"
```

Buatlah bash script untuk mempopulate sample data dummy. Misal `touch /var/lib/postgresql/populate`, buka dengan text editor dan isikan scrip dibawah ini:

```bash
#!/usr/bin/bash

# Isi data dummy awal
for n in {1..10}; do
        psql db_dummy -c "INSERT INTO sample (amount) VALUES ($RANDOM);"
        sleep 1
done

# Tampilkan data exisiting (optonal)
# psql db_dummy -c "SELECT * FROM sample;"
```

Ubah mode file sehingga bersifat _executable_, eksekusi file tersebut, dan catat `timestamp` eksekusinya:

```bash
# Ubah mode script menjadi executable
chmod u+x /var/lib/postgresql/populate

# Eksekusi script dan catat timestamp eksekusinya
/var/lib/postgresql/populate
```

Misalkan timestamp eksekusinya adalah pada `2022-03-15 14:00:00.000`, maka catat timestamp ini karena akan kita pakai nantinya.

## Menjalankan Base Backup

Buat direktori untuk backup (user: `postgres`):

```bash
mkdir -vp /var/lib/postgresql/backup/12/main
```

Jalankan base backup:

```bash
pg_basebackup -D /var/lib/postgresql/backup/12/main -z -Ft
```

## Simulasikan penambahan data baru

Eksekusi scipt penambahan data yang sudah kita buat sebelumnya (`/var/lib/postgresql/populate`) beberapa kali namun beri jeda waktu berbeda setiap eksekusinya (misal per 10 menit), catat waktu eksekusinya, dan catat keadaan data pada setiap eksekusi. Misal (user: `postgres`):

```bash
# Eksekusi script peambahan data
/var/lib/postgresql/populate

# Print keadaan tabel, dan outputkan 
# berupa file dengan timestamp sebagai 
# penanda waktunya
psql db_dummy -c "SELECT * FROM sample;" > table-sample-at-$(date +\%y\%m\%d-%H\%M\%S).txt
```

## Simulasikan insiden kehilangan data

Asumsi kita kehilangan 5 data awal dan 5 data akhir (user: `postgres`):

```bash
# Simulasi kehilangan 5 data awal
psql db_dummy -c "DELETE FROM sample WHERE id IN (SELECT id FROM sample ORDER BY id LIMIT 5)";

# Simulasi kehilangan 5 data terakhir
psql db_dummy -c "DELETE FROM sample WHERE id IN (SELECT id FROM sample ORDER BY id DESC LIMIT 5)";
```

Print keadaan terakhir tabel, dan outputkan berupa file dengan timestamp sebagai penanda waktunya:

```bash
psql db_dummy -c "SELECT * FROM sample;" > table-sample-at-$(date +\%y\%m\%d-%H\%M\%S).txt
```

## PITR in Action

### Stop Cluster

Hentikan cluster terkait (gunakan user dengan privilege `sudo`):

```bash
sudo systemctl stop postgresql@12-main
```

### Hapus seluruh data cluster

Hapus seluruh data di cluster (user: `postgres`):

```bash
# Masuk ke direktori data cluster
cd /var/lib/postgresql/12/main

# Hapus semuanya
rm -rf *
```

### Restore data cluster dari base backup

Pasa [step sebelumnya](#menjalankan-base-backup) kita sudah membuat backup. Sekarang restore data cluster dari file-file backup tersebut (user: `postgres`):

```bash
# Restore data dari file base.tar.gz
tar -vxz -f /var/lib/postgresql/backup/12/main/base.tar.gz -C /var/lib/postgresql/12/main

# Restore data dari file pg_wal.tar.gz
tar -vxz -f /var/lib/postgresql/backup/12/main/pg_wal.tar.gz -C /var/lib/postgresql/12/main/pg_wal
```

Direktori `/var/lib/postgresql/backup/12/main` akan terisi kembali dengan data dari file-file backup.

### Konfigurasi mode recovery

Sebelum menjalankan kembali cluster, kita harus mengkonfigurasi beberapa option untuk mode recovery PG. Buat sebuah file didalam `/etc/postgresql/12/main/conf.d` bernama `recovery.conf` dan isikan pengaturan berikut (user: `postgres`):

```ini
restore_command         = 'cp /var/lib/postgresql/archives/12/main/%f %p'
archive_cleanup_command = 'pg_archivecleanup /var/lib/postgresql/archives/12/main %r'
recovery_target_time    = '2022-03-12 06:15:00.000'
```

Khusus untuk option `recovery_target_time`, isikan value dengan timestamp keadaan dimana kita belum kehilangan data. Pada contoh diatas kita set ke `2022-03-12 06:15:00.000`.

Setelah membuat file konfigurasi untuk recovery, kita perlu membuat suatu file kosong bernama `recovery.signal` didalam direktori data cluster yang akan memicu PG untuk berjalan pada ke mode recovery sesaat setelah di-_start_ (user: `postgres`):

```bash
touch /var/lib/postgresql/12/main/recovery.signal
```

### Jalankan kembali cluster

Pada step ini kita akan menjalankan cluster agar masuk ke mode recovery. Namun sebelum itu ada baiknya kita mengaktifkan fungsi untuk memantau aktifitas cluster melalui file log PG. Gunakan shell session terpisah dan eksekusi (gunakan user dengan  privilege `sudo`):

```bash
sudo tail -f /var/log/postgresql/postgresql-12-main.log
```

Setelah log file terpantau, barulah kita jalankan cluster (gunakan user dengan  privilege `sudo`):

```bash
sudo systemctl start postgresql@12-main
```

Jika tidak ada error saat memulai cluster, perhatikan log dan kita akan menemukan baris-baris yang bertulisakan:

```log
...
LOG:  starting point-in-time recovery to 2022-03-12 06:15:00+00
...
...
LOG:  recovery has paused
HINT: Execute pg_wal_replay_resume() to continue.
```

Sampai disini langkah terakhir yang perlu kita lakukan adalah menjalankan command yang di rekomendasikan pada log diatas (user: `postgres`):

```bash
psql -c "SELECT pg_wal_replay_resume();"
```

Perhatikan log lagi, jika pada log terdapat baris bertuliskan `LOG:  database system is ready to accept connections` maka kita sudah berhasil melakukan PITR. Dan jangan lupa untuk periksa kembali keadaan tabel `sample` (user: `postgres`):

```bash
psql db_dummy -c "SELECT * FROM sample;"
```

Jika data yang hilang sudah kembali ke keadaan semula berarti PITR sudah berjalan dengan benar.
