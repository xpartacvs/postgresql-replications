# PostgreSQL 12 - Point-in-Time Recovery (PITR) dengan `pg_receivewal`

PITR adalah fitur PG yang memungkinkan DBA mengembalikan keadaan suatu cluster PG pada suatu titik waktu tertentu.

## Tahap Persiapan

- 2 server ubuntu 20.04 dalam satu jaringan yang sama
- Masing-masing server sudah terinstall PostgreSQL 12
- Server Utama (MDE) kita asumsikan memiliki IP **10.10.1.10**
- Server Replica (RDE) kita asumsikan memiliki IP **10.10.1.20**

### Persiapan tambahan RDE

#### Install Server NFS

NFS kita butuhkan untuk membuat network direktori (shared) yang menghubungkan MDE dan RDE. Direktori ini nantinya akan menjadi media transfer backup dan WAL files antara MDE dan RDE ketika kita perform PITR.

```bash
# Buat sebuah direktory yang akan kita gunakan sebagai network direktori
sudo mkdir -vp /var/nfs/sharedir

# Ubah ownershio direktori tersebut menjadi nobody:nogroup
sudo chown -vRf nobody:nogroup /var/nfs/sharedir

# Install server NFS
sudo apt update
sudo apt -y install nfs-kernel-server
```

Setelah server NFS terinstall, konfigurasi share directory dengan meng-_edit_ file `/etc/exports` (misal `sudo /etc/exports`) dan isikan script berikut (jangan lupa simpan setelahnya):

```
/var/nfs/sharedir   10.10.1.10(rw,sync,no_subtree_check,no_root_squash)
```

Kemudian jalankan 2 _(dua)_ command dibawah ini:

```bash
# Restart server NFS
sudo systemctl restart nfs-kernel-server

# Propagate share directory
exportfs -va
```

#### Install Supervisord

Supervisord akan kita gunakan untuk menjalankan `pg_receivewal` sebagai background process:

```bash
sudo apt update
sudo apt -y install supervisor
```

### Persiapan tambahan di MDE

#### Install NFS Client

NFS client kita butuhkan untuk terhubung dengan NFS server yang ada di RDE:

```bash
# Buat sebuah direktory yang akan kita mount 
# dengan network directory NFS
sudo mkdir -vp /mnt/nfs/sharedir

# Ubah ownershio direktori tersebut menjadi nobody:nogroup
sudo chown -vRf nobody:nogroup /mnt/nfs/sharedir

# Install NFS Client
sudo apt update
sudo apt -y install nfs-common
```

Seletah NFS Client terinstall edit file `/etc/fstab` (misal: `sudo vim /etc/fstab`) dan tambahkan baris berikut ini (jangan lupa simpan setelahnya):

```
10.10.1.20:/var/nfs/sharedir    /mnt/nfs/sharedir   nfs4    auto,noatime,nolock,bg,nfsvers=4,intr,tcp,actimeo=1800  0   0
```

Kemudain restart MDE:

```bash
sudo reboot
```

Dan periksa apakah direktori `/var/nfs/sharedir` pada RDE sudah berhasil ter-_mount_ dengan direktory `/mnt/nfs/sharedir` di MDE dengan menjalankan command:

```bash
sudo df -h
```

## Mem-_populate_ Data Sample di MDE

Buat sebuah database dan table untuk data dummy (gunakan user `postgres`):

```bash
# Buat database db_dummy
psql -c "CREATE DATABASE db_dummy;"

# Buat tabel sample
psql db_dummy -c "CREATE TABLE sample (id serial not null, amount int8 not null, time_create timestamp not null default current_timestamp);"
```

Buatlah bash script untuk mempopulate sample data dummy. Misal `touch /var/lib/postgresql/populate`, buka dengan text editor dan isikan scrip dibawah ini:

```bash
#!/usr/bin/bash

# Isi data dummy awal
for n in {1..10}; do
        psql db_dummy -c "INSERT INTO sample (amount) VALUES ($RANDOM);"
        sleep 1
done

# Tampilkan data exisiting (optonal)
# psql db_dummy -c "SELECT * FROM sample;"
```

Ubah mode file sehingga bersifat _executable_, eksekusi file tersebut, dan catat `timestamp` eksekusinya:

```bash
# Ubah mode script menjadi executable
chmod u+x /var/lib/postgresql/populate

# Eksekusi script dan catat timestamp eksekusinya
/var/lib/postgresql/populate
```

Misalkan timestamp eksekusinya adalah pada `2022-03-15 14:00:00.000`, maka catat timestamp ini karena akan kita pakai nantinya.

## Menjalankan `pg_receivewal`

### Membuat replication slot di MDE

`pg_receivewal` berguna untuk menarik WAL files dari MDE. Untuk dapat memanfaatkan `pg_receivewal` kita harus membuat replication slot (syarat mutlak penggunaan `pg_receivewal`) di MDE. Kita sepakati replication slot bernama `receivewal` (:warning: pastikan menggunakan user `postgres`):

```bash
# Lakukan ini di MDE
psql -c "SELECT * FROM pg_create_physical_replication_slot('receivewal');"

# Lihat status replication slot (optional)
# psql -c "SELECT * FROM pg_replication_slots;"
```

### Membuat direktori penampung WAL files dan backup

Kita harus menentukan dimana WAL file yang kita tarik dari MDE tersimpan di RDE. Oleh karena itu kita buat direktori baru yang akan menampung WAL files tersebut (:warning: pastikan menggunakan user `postgres`):

```bash
# Buat direktri penampung WAL files
mkdir -vp ~/archives/12/receivewal
```

Selain itu kita juga butuh direktori untuk menyimpan file base-backup (:warning: pastikan menggunakan user `postgres`):

```bash
# Buat direktory penampung file backup
mkdir -vp ~/backup
```

### Konfigurasi `pg_receivewal` sebagai background process

Secara default jika kita mejalankan`pg_receivewal` maka ia akan berjalan di _foreground_. Sedankan kita butuh `pg_receivewal` untuk berjalan sebagai _background_ process. Oleh karena itu kita akan memanfaatkan supervisord yang sudah kita install di [tahap persiapan](#install-supervisord).

Buat sebuah file konfigurasi supervisord (:warning: jangan gunakan user `postgres`, gunakan user yang memiliki privilege `sudo`):

```bash
sudo vim /etc/supervisor/conf.d/receivewal.conf
```

Kemudian isikan syntax berikut ini (jangan lupa simpan setelahnya):

```ini
[program:receivewal]
user                    = postgres
directory               = /var/lib/postgresql
command                 = pg_receivewal -D /var/lib/postgresql/archives/12/receivewal -S receivewal -h 10.10.1.10 -U replicator -w -v
stdout_logfile          = /var/log/supervisor/receivewal-out.log
stdout_logfile_maxbytes = 10MB
stdout_logfile_backups  = 10
stderr_logfile          = /var/log/supervisor/receivewal-err.log
stderr_logfile_maxbytes = 10MB
stderr_logfile_backups  = 10
```

Kemudian eksekusi perintah-perintah dibawah ini agar `pg_receivewal` berjalan sebagai background process:

```bash
sudo supervisorctl reread
sudo supervisorctl add receivewal:*
sudo supervisorctl status all
```

Jika `pg_receivewal` sukses berjalan sebagai background process, makan direktori `/var/lib/postgresql/archives/12/receivewal` akan mulai terisi dengan WAL files.

### Membackup database

Setelah `pg_receivewal` suskses berjalan di background selanjutnya kita perlu membuat base backup dari database (:warning: pastikan menggunakan user `postgres`):

```bash
pg_basebackup -h 10.10.1.10 -p 5432 -U replicator -w -Xn -Ft -D - | bzip2 > ~/backup/backup-$(date +\%y\%m\%d-%H\%M\%S).tar.bz2
```

File base backup ini nantinya akan kita gunakan saat restore dengan mengkombinasikannya dengan WAL files.

## Skenario Perubahan Data

### Simulasikan penambahan data baru

Di MDE, eksekusi scipt penambahan data yang sudah kita buat sebelumnya (`/var/lib/postgresql/populate`) beberapa kali namun beri jeda waktu berbeda setiap eksekusinya (misal per 10 menit), catat waktu eksekusinya, dan catat keadaan data pada setiap eksekusi. Misal (user: `postgres`):

```bash
# Eksekusi script peambahan data
/var/lib/postgresql/populate

# Print keadaan tabel, dan outputkan 
# berupa file dengan timestamp sebagai 
# penanda waktunya
psql db_dummy -c "SELECT * FROM sample;" > table-sample-at-$(date +\%y\%m\%d-%H\%M\%S).txt
```

### Simulasikan insiden kehilangan data

Asumsi kita kehilangan 5 data awal dan 5 data akhir (user: `postgres`):

```bash
# Simulasi kehilangan 5 data awal
psql db_dummy -c "DELETE FROM sample WHERE id IN (SELECT id FROM sample ORDER BY id LIMIT 5)";

# Simulasi kehilangan 5 data terakhir
psql db_dummy -c "DELETE FROM sample WHERE id IN (SELECT id FROM sample ORDER BY id DESC LIMIT 5)";
```

Print keadaan terakhir tabel, dan outputkan berupa file dengan timestamp sebagai penanda waktunya:

```bash
psql db_dummy -c "SELECT * FROM sample;" > table-sample-at-$(date +\%y\%m\%d-%H\%M\%S).txt
```

## PITR in Action

### Stop Cluster di MDE

Hentikan cluster terkait (gunakan user dengan  privilege `sudo`):

```bash
sudo systemctl stop postgresql@12-main
```

### Hapus seluruh data cluster di MDE

Hapus seluruh data di cluster (user: `postgres`):

```bash
# Masuk ke direktori data cluster
cd /var/lib/postgresql/12/main

# Hapus semuanya
rm -rf *
```

### Restore Database

Di RDE copy WAL files dan file backup ke sharedir NFS:

```bash
# Copy WAL Files ke sharedir NFS
sudo cp -vRip /var/lib/postgresql/archives/12/receivewal /var/nfs/sharedir/

# Copy file base backup ke sharedir NFS
sudo cp -vRip /var/lib/postgresql/backup/backup.bz2 /var/nfs/sharedir/
```

Di MDE, extract file base backup yang ada di sharedir NFS ke direktori utama cluster (user: `postgres`):

```bash
tar -vxj -f /mnt/nfs/sharedir/backup.tar.bz2 -C /var/lib/postgresql/12/main
```

Di MDE, copy juga seluruh WAL files yang ada di sharedir NFS ke direktory WAL files cluster MDE (user: `postgres`):

```bash
cp -vRip /mnt/nfs/sharedir/receivewal/* /var/lib/postgresql/archives/12/main
```

> **Penting:**
> Setelah seluruh WAL files dicopy, hilangkan ekstensi seluruh WAL file berekstensi `.partial` yang ada di `/var/lib/postgresql/archives/12/main` (jika ada). Misal ada file `0000700002F.partial` maka rename menjadi `0000700002F`

### Konfigurasi mode recovery

Sebelum menjalankan kembali cluster, kita harus mengkonfigurasi beberapa option untuk mode recovery PG. Buat sebuah file didalam `/etc/postgresql/12/main/conf.d` bernama `recovery.conf` dan isikan pengaturan berikut (user: `postgres`):

```ini
restore_command         = 'cp /var/lib/postgresql/archives/12/main/%f %p'
archive_cleanup_command = 'pg_archivecleanup /var/lib/postgresql/archives/12/main %r'
recovery_target_time    = '2022-03-12 06:15:00.000'
```

Khusus untuk option `recovery_target_time`, isikan value dengan timestamp keadaan dimana kita belum kehilangan data. Pada contoh diatas kita set ke `2022-03-12 06:15:00.000`.

Setelah membuat file konfigurasi untuk recovery, kita perlu membuat suatu file kosong bernama `recovery.signal` didalam direktori data cluster yang akan memicu PG untuk berjalan pada ke mode recovery sesaat setelah di-_start_ (user: `postgres`):

```bash
touch /var/lib/postgresql/12/main/recovery.signal
```

### Jalankan kembali cluster

Pada step ini kita akan menjalankan cluster agar masuk ke mode recovery. Namun sebelum itu ada baiknya kita mengaktifkan fungsi untuk memantau aktifitas cluster melalui file log PG. Gunakan shell session terpisah dan eksekusi (gunakan user dengan  privilege `sudo`):

```bash
sudo tail -f /var/log/postgresql/postgresql-12-main.log
```

Setelah log file terpantau, barulah kita jalankan cluster (gunakan user dengan  privilege `sudo`):

```bash
sudo systemctl start postgresql@12-main
```

Jika tidak ada error saat memulai cluster, perhatikan log dan kita akan menemukan baris-baris yang bertulisakan:

```log
...
LOG:  starting point-in-time recovery to 2022-03-12 06:15:00+00
...
...
LOG:  recovery has paused
HINT: Execute pg_wal_replay_resume() to continue.
```

Sampai disini langkah terakhir yang perlu kita lakukan adalah menjalankan command yang di rekomendasikan pada log diatas (user: `postgres`):

```bash
psql -c "SELECT pg_wal_replay_resume();"
```

Perhatikan log lagi, jika pada log terdapat baris bertuliskan `LOG:  database system is ready to accept connections` maka kita sudah berhasil melakukan PITR. Dan jangan lupa untuk periksa kembali keadaan tabel `sample` (user: `postgres`):

```bash
psql db_dummy -c "SELECT * FROM sample;"
```

Jika data yang hilang sudah kembali ke keadaan semula berarti PITR sudah berjalan dengan benar.
