# PostgreSQL 12  - Streaming Replication

## Tahap Persiapan

Pada tahap ini kita perlu memastikan terpenuhinya **minumum requirement** berikut ini:

- [x] 2x server Ubuntu 20.04 dengan spec cukup
- [x] Kedua server berada pada network yang sama
- [x] Kedua server sudah terinstall PostgreSQL 12

Selain itu untuk memudahkan pemahaman dan mempersingkat teks pada tutorial ini kita juga akan menyepakati beberapa asumsi terkait istilah-istilah yang akan kita gunakan:

- Istilah "Database" untuk kedepannya akan kita singkat dengan **DB**
- Istilah "PostgreSQL 12" untuk kedepannya akan kita ganti dengan **PG**
- Istlah "server" untuk kedepannya akan kita ganti dengan **Node**
- Node pertama yang akan kita gunakan sebagai server db utama seterusnya akan kita sebut sebagai **MDE** _(main node)_
- Node kedua yang akan kita gunakan sebagai server db replica seterusnya akan kita sebut sebagai **RDE** _(replica node)_
- MDE kita asumsikan memiliki IP **10.10.1.10**
- RDE kita asumsikan memiliki IP **10.10.1.20**

### Periksa kesiapan cluster PG di masing-masing node

Saat PG pertama kali diinstal pada Ubuntu, biasanya PG akan membuat sebuat cluster default bernama **main** dan langsung akan menjalankannya. **Cluster** pada PG bisa diibaratkan sebagai suatu background service database independent (satu sama lain) yang berjalan diatas port tertentu _(default 5432)_ yang melayani permintaan operasional database.

#### Melihat daftar cluster

Untuk dapat melihat daftar cluster yang ada (berikut statusnya) pada suatu node, kita dapat menjalankan perintah berikut ini di terminal/console _(silahkan coba dimasing-masing node)_:

```bash
sudo pg_lsclusters
```

Contoh output:

```bash
Ver   Cluster Port    Status  Owner       Data directory              Log file
12    main    5432    online  postgress   /var/lib/postgresql/12/main /var/log/postgresql/postgresql-12-main.log
```

| **Kolom**      | **Keterangan**                                               |
| :---           | :---                                                         |
| Ver            | Versi dari PG yang mengelola cluster                         |
| Cluster        | Nama cluster                                                 |
| Port           | Nomor port yang digunkan oleh cluster untuk menerima request |
| Status         | Status cluster                                               |
| Owner          | User yang memiliki dan memicu jalannya cluster               |
| Data directory | Path ke direktori tempat data-data cluster tersimpan         |
| Log file       | Path ke file log dari cluster                                |

#### Membuat cluster baru

Jika ternyata cluster tidak ada, kita bisa membuat cluster baru dengan menggunakan command `pg_createcluster` seperti contoh dibawah ini:

```bash
sudo pg_createcluster 12 main --start
```

Jika command diatas dijalankan, maka PG akan membuat sebuah cluster versi `12` baru benama `main` sekaligus langsung menjalankannya (efek dari option `--start`).

> **Tips:**
>
> - Gunakan command `man pg_createcluster`untuk melihat panduan penggunaan command tersebut.
> - Untuk memastikan keadaan cluster, periksa kembali cluster yang sudah dibuat dengan command `sudo pg_lsclusters`

#### Mengubah nama cluster

Sampai disini kita sudah belajar cara melihat daftar cluster dengan command `pg_lsclusters` dan membuat cluster baru dengan command `pg_createcluster`. Selanjutnya kita akan belajar cara mengubah nama cluster. Wait..! Untuk apa?

Jika kita menjalankan perintah `pg_lsclusters` di masing-masing node (MDE dan RDE), maka akan kita dapati bahwa kedua cluster yang ada pada masing-masing node memiliki nama yang sama yaitu `main`. Oleh karena itu untuk menghidari kerancuan kedepannya, kita akan ubah nama cluster yang berada pada RDE (agar lebih sesuai dengan peruntukannya sebagai replika) menjadi `replica`.

Command yang akan kita gunakan untuk mengubah nama cluster adalah `pg_renamecluster`. Sekarang jalankan terminal command berikut ini pada RDE:

```bash
sudo pg_renamecluster 12 main replica
```

> **Tips**:
>
> - Gunakan command `man pg_renamecluster` untuk melihat panduan penggunaan command tersebut.

Setelah command diatas kita jalankan pada RDE, maka PG akan merubah nama cluster dari `main` menjadi `replica` (dengan terlebih dahulu menghentikan background processnya baru kemudian dijalankan lagi setelah nama cluster berubah).

Dan tidak hanya itu, PG juga merubah path ke diretori data cluster, konfigurasi dan juga path ke log cluster. Hal ini dapat kita buktikan dengan menampilkan ulang list cluster menggunakan command `pg_lsclusters`:

| **Kolom**        | **Sebelum `pg_renamecluster`**               | **Sesudah `pg_renamecluster`**                  |
| :---             | :---                                         | :---                                            |
| Cluster          | `main`                                       | `replica`                                       |
| Data Directory   | `/var/lib/postgresql/12/main`                | `/var/lib/postgresql/12/replica`                |
| Config Directory | `/etc/postgresql/12/main`                    | `/etc/postgresql/12/replica`                    |
| Log file         | `/var/log/postgresql/postgresql-12-main.log` | `/var/log/postgresql/postgresql-12-replica.log` |

> :warning: **Perhatian**:
>
> Sampai disini kita akan menyepakati beberapa asumsi tambahan yaitu:
>
> - Istilah `main` yang merujuk pada cluster di MDE untuk seterusnya akan kita ganti menjadi **MCL**.
> - Istilah `replica` yang merujuk pada cluster di RDE untuk seterusnya akan kita ganti menjadi **RCL**.

#### Menghapus cluster

Ada kalanya kita berada disituasi dimana kita diharuskan menghapus suatu cluster di PG. Part ini tidak berkaitan langsung dengan kebutuhan kita terhadap tahap persiapan dalam membangun solusi stream replication, melainkan hanya sebagai pelengkap dari 3 operasi cluster yang sudah kita pelajari diatas.

Unutk dapat melakukan operasi penghapusan cluster di PG, kita dapat menggunakan command `pg_dropcluster` seperti contoh dibawah ini:

```bash
sudo pg_dropcluster --stop 12 broken-cluster
```

Jika command diatas dijalankan, maka PG akan menghapus cluster versi 12 bernama `broken-cluster` dengan terlebih dahulu menghentikan background process-nya (efek option `--stop`).

> **Tips**:
>
> - Gunakan command `man pg_dropcluster` untuk melihat detail cara penggunaan command tersebut.
> - Untuk memastikan keadaan cluster, periksa kembali dengan command `sudo pg_lsclusters`

### Periksa ketersediaan dedicated user PG

Selain cluster, saat pertama kali diinstal di Ubuntu idealnya PG juga akan membuat sebuah dedicated user dan group (dilevel OS) bernama `postgres`. User ini idealnya adalah user yang sebaiknya kita gunakan untuk operasi-operasi bersifat administratif di lingkup PG.

Cara paling tepat untuk memeriksa ketersedian user `postgres` ialah dengan _switch-as_ user tersebut menggunakan perintah `su` seperti contoh berikut ini:

```bash
sudo su - postgres
```

Jika tidak muncul pesan error, artinya kita sudah berhasil switch-as user `postgres`. Untuk memastikannya kita bisa jalankan command `whoami` (yang mana command ini akan mencetak username yang saat itu sedang kita gunakan). Sebaliknya jika muncul pesan error `su: user postgres does not exist` artinya user tersebut tidak ada atau gagal dibuat saat proses instalasi PG.

#### Periksa home directory milik user `postgres`

Setelah memastikan ketersediaan user `postgres` selanjutnya kita perlu memastikan ketersedian dan kepemilikan home directory dari user `postgres`. Pada ubuntu, home directory default dari user `postgres` adalah `/var/lib/postgresql`. Untuk memastikan kebenarannya, kita bisa memanfaatkan command `echo` untuk mencetak home direktori. Contoh `echo ~` atau `echo $HOME`. Jika yang tercetak pada terminal adalah `/var/lib/postgresql`maka home ditectory sudah benar (secara default).

## Tahap Eksekusi

Setelah melewati tahap persiapan yang lumayan panjang, sekarang waktunya kita mengeksekusi pembuatan streaming replication di PG.

### Membuat direktori penampung WAL files

Langkah eksekusi kita diawali dengan membuat direktori untuk menampung file-file WAL _(Write-ahead Log)_ di kedua node. Namun sebelum melakukannya pastikan kita menggunakan user `postgres`. Jika belum, switch user kita dengan command berikut ini:

```bash
sudo su - postgres
```

Kemudian eksekusi perintah berikun ini di MDE:

```bash
mkdir -vp /var/lib/postgresql/log_archives/12/main
```

Lakukan juga di RDE namun buat agar nama direktori sama dengan nama clusternya:

```bash
mkdir -vp /var/lib/postgresql/log_archives/12/replica
```

### Membuat user dengan privilege replication

Untuk dapat mereplika MCL, RCL harus menggunakan user yang memiliki privilege replication. User ini kita sepakati bernama `replicator` dan ini harus dibuat dulu di MDE (meskipun nantinya user ini juga akan terdapat pada RDE dengan sendirinya setelah poses replikasi berjalan lancar). Untuk melakukannya kita akan memanfaatkan command `psql` (PG shell/console):

```bash
psql -c "CREATE USER replicator WITH REPLICATION;"
```

### Mengkonfigurasi akses network

Setelah user `replicator` berhasil dibuat, user ini belum bisa langsung digunakan oleh RCL untuk dapat mereplika MCL. Ada 2 langkah tambahan yang perli dilakukan yaitu:

- [ ] Membuka akses network; dan
- [ ] Mendaftarkan user `replicator` pada Host-based Authentication atau **HBA**

#### Seklilas tentang pengelolaan konfigurasi cluster PG di Ubuntu

Sebelum melakukan 2 langkah tambahan diatas, ada baiknya jika kita memahami bagaimana PG mengelola konfigurasi cluster.

Pada Ubuntu, secara default area konfigurasi untuk setiap cluster berada di direktori dengan format path **_/etc/postgresql/\<versi-cluster\>/\<nama-cluster\>_**. Jika kita masuk kedalamnya _(misal pada MDE path direktorinya adalah `/etc/postgresql/12/main`)_, maka akan terdapat:

- Beberapa file ber-ekstensi `.conf`.
- Sebuah file bernama `environment`; dan
- sebuah direktori bernama `conf.d`.

> :warning: **Perhatian :**
>
> - Perhatikan  bahwa direktori tersebut beserta isinya dimiliki oleh user `postgres`.
> - Sementara kita akan kesampingkan terlebih dahulu file `environment` sehingga fokus kita adalah pada file-file dengan ekstensi `.conf` dan direktori `conf.d`.

File ber-ekstensi `.conf` adalah file yang berisi pengaturan terhadap cluster terkait. Sebagian besar pengaturan ada dialam file `postgresql.conf`. Lalu bagaimaa dengan direktori `conf.d`? Direktori `conf.d` adalah direktori yang sengaja disiapkan oleh PG pada ubuntu untuk menampung pengaturan-pengaturan custom atau non-default dari file `postgresql.conf` yang nantinya dibuat oleh DBA (database administrator).

Sampai disini mungkin muncul pertanyaan dibenak kita: _Kenapa harus memisahkan pengaturan custom (non-default)? Kenapa tidak langsung saja memodifikasi file `postgresql.conf`?_ Jawabannya adalah _tidak harus_, dan benar bahwa pengaturan custom dapat dilakukan dengan cara memodifikasi file `postgresql.conf` secara langsung. **Namun**: Adanya direktori `conf.d` adalah inisiatif yang nantinya akan memudahkan DBA dalam mengelola konfigurasi pada cluster.

Sebagai contoh, misalkan seorang DBA perlu membuka akses network untuk sementara waktu. Maka ia perlu merubah value konfigurasi `listen_addresses` dari `localhost` (default value) menjadi `*`. Perubahan konfigurasi ini sebenarnya bisa saja langsung diterapkan dengan memodifikasi file `postgresql.conf` namun konsekuensinya (setidaknya) ia akan kehilangan catatan bahwa default value dari `listen_addresses` adalah `localhost`. Disinilah direktori `conf.d` berperan.

Alih-alih harus merubah default value pada file `postgresql.conf`, DBA punya opsi untuk  menuliskan konfigurasi custom pada sebuah file baru (misal: `networks.conf`) dan menyimpannya didalam direktofi `cond.d`. Sewaktu-waktu jika DBA sudah tidak membutuhkan pengaturan custom tersebut ia dapat mengembalikannya ke default value dengan hanya menghilangkan ekstensi `.conf` pada file `networks.conf`. Dengan begitu DBA mendapatkan beberapa keuntungan:

- DBA tidak kehilangan catatan default value yang tersimpan di file `postgresql.conf`.
- DBA juga tidak kehilangan catatan dari pengaturan custom yang pernah diterapkannya.

#### Membuka akses network

Secara default PG di ubuntu tidak membuka akses network. Atau lebih tepatnya akses ke database hanya bisa dilakukan dari dalam host tempat PG terinstall. Sedangkan stream replication yang hendak kita buat membutuhkan (setidaknya) 2 node yang saling terhubung melalui network. Oleh karena itu kita perlu membuka akses network di kedua node yang kita gunakan (MDE & RDE).

Buatlah sebuah file baru (misal: `networks.conf`) di dalam direktori `conf.d` dimasing-masing node (pastikan melakukannya dengan menggunakan user `postgres`):

```bash
# Lakukan ini di MDE
touch /etc/postgresql/12/main/conf.d/networks.conf

# Lakukan ini di RDE
touch /etc/postgresql/12/replica/conf.d/networks.conf
```

Kemudian buka file `networks.conf` di masing-masing node dan tuliskan baris berikut ini didalamnya (jangan lupa simpan setelahnya) :

```ini
listen_addresses = '*'
```

#### Mengkonfigurasi Host-based Authentication

Setelah membuka akses network dimasing-masing node, langkah selanjutnya adalah mendaftarkan user `replicator` pada Host-based Authentication (HBA).

Di Ubuntu HBA, diatur dalam sebuah file bernama `pg_hba.conf` yang berada didalam direktori dengan format path **_/etc/postgresql/\<versi-cluster\>/\<nama-cluster\>_**. Buka file tersebut dengan text edior seperti `vim`,`nano`, dan sejenisnya. Contoh: `vim /etc/postgresql/12/main` (lakukanlah di MCL dan di RCL). Didalam file `pg_hba.conf` kita akan melihat baris-baris teks yang tersesun menyerupai tabel dengan 5 kolom.

Tambahkan baris dibawah ini sebagai baris paling terakhir dari file tersebut (mengikuti pola yang ada), kemudian simpan dan tutup kembali file tersebut:

```bash
host    replication     replicator      10.10.1.1/24    trust
```

Dengan menuliskan baris diatas kita menginformasikan pada HBA bahwa akan ada satu atau lebih eksternal host  yang akan mengakses cluster untuk keperluan `replication` menggunakan user bernama `replicator` dari source IP antara `10.10.1.1 s/d 10.10.1.255` (CIDR `10.10.1.1/24`) yang harus diizinkan (`trust`).

> **Tips**: Lihat [dokumentasi official HBA](https://www.postgresql.org/docs/12/auth-pg-hba-conf.html)

### Mengkonfigurasi Streaming Replication

Pada direktori yang sama tempat file `pg_hba.conf` berada, juga terdapat file bernama `postgresql.conf`. File ini adalah file konfigurasi utama dari suatu cluster yang berjalan di PG.

Untuk melakukan perubahan konfigurasi, umumnya dilakukan pada file tersebut. Namun pada Ubuntu, modifikasi konfigurasi disarankan dilakukan dengan cara membuat file berekstensi `.conf` baru dan meletakannya didalan directori `conf.d`. Hal ini disarankan agar kita lebih mudah mengelola pengaturan (contoh: ). misalnya:

- Kita akan dapat membedakan mana konfigurasi default dan mana yang custom
- Ketika salah dalam melakukan konfigurasi dan kita ingin mengembalikanya kekeadaan semula kita bisa melakukannya hanya dengan merubah ekstensi file (yang salah tersebut) sehingga tidak berekstensi `.conf`
- dan sebagainya

#### Mengkonfigurasi log archiving

Sekarang buatlah sebuah file bernama `log-archiving.conf` (lakukan di MDE dan RDE) didalam direktori `conf.d` dan isikan:

```ini
archive_mode        = on
archive_command     = 'test ! -f /var/lib/postgresql/log_archives/12/main/%f && cp %p /var/lib/postgresql/log_archives/12/main/%f' 
```

> :warning: Pada `archive_command` pastikan path direktori archive sesuai dengan yang sudah kita buat di masing-masing node di [step pembuatan direktori archive](#membuat-direktori-penampung-wal-files)

#### Membuat replication slot

Repliction Slot adalah fitur PG (versi 9.4 keatas) yang memungkinkan MCL menyimpan WAL files (dan menunda pemghapusannya) ketika (dan selama) RCL tidak terhubung dengan MCL (entah karena failure atau hal lain). Fitur ini menjadi penting karena RCL nantinya akan bisa mengejar ketinggalannya terhadap state data terbaru yang ada di MCL selagi RCL tidak/gagal terhubung ke MCL.

Untuk membuat replication slot, eksekusi command berikut ini di MDE (:warning: pastikan menggunakan user `postgres`):

```bash
# Lakukan ini di MDE
psql -c "SELECT * FROM pg_create_physical_replication_slot('replslot');"

# Lihat status replication slot (optional)
# psql -c "SELECT * FROM pg_replication_slots;"
```

Dengam mengeksekusi command diatas kita sepakati bahwa replication slot yang kita buat bernama `replslot`.

> :warning: **PENTING**
>
> - Selama RCL tidak terhubung dengan MCL, maka WAL files akan terus disimpan dan ukurannya akan terus membengkak sehingga MDE berresiko kehabisan kapasitas storage.
> - Ketika ada suatu kondisi dimana sebuah node replica sudah tidak akan digunakan lagi, replication slot yang terkait dengan node tersebut harus dihapus. Jika tidak, PG di node utama akan mengira replica **hanya belum terkoneksi** dan terus menyimpan WAL files sampai kehabisan kapasitas storage.
> - Replication slot yg sedang aktif tidak bisa langsung dihapus. Untuk bisa menghapusnya kita perlu menghentikan dulu proses yg sedang memakai slot tsesebut (mungkin dengan menghentikan proses `pg_receivewall` atau dengan memodifikasi `recovery.conf` supaya tidak memakai slot lagi)
> - Untuk menghapus suatu replication slot, jalankan command (pastikan menggunakan user `postgres`): `psql -c "SELECT pg_drop_replication_slot('nama-replication-slot');"`

#### Mengkonfigurasi mode recovery

Mode recovery adalah mode yang berjalan ketika MCL mengalamai kegagalan dan RCL harus mengambil peran sebagai database utama. Pada mode ini dibutuhkan mekanisme untuk merestore data dengan memanfaatkan WAL files.

Buatlah sebuah file baru (dan beri nama misal: `recovery.conf`) didalam direktori `conf.d` di MDE dan RDE dan isikan baris-baris seperti dibawah ini:

File `/etc/postgresql/12/main/conf.d/recovery.conf` di MDE:

```ini
restore_command         = 'cp /var/lib/postgresql/archives/12/main/%f %p'
archive_cleanup_command = 'pg_archivecleanup /var/lib/postgresql/archives/12/main %r'

# Secara default sudah diset ke 'latest'
# recovery_target_timeline = 'latest'

# di PG dgn versi < 12, standy mode perlu diset
# standby_mode = 'on'
```

File `/etc/postgresql/12/replica/conf.d/recovery.conf` di RDE:

```ini
restore_command         = 'cp /var/lib/postgresql/archives/12/replica/%f %p'
archive_cleanup_command = 'pg_archivecleanup /var/lib/postgresql/archives/12/replica %r'

# Secara default sudah diset ke 'latest'
# recovery_target_timeline = 'latest'

# di PG dgn versi < 12, standy mode perlu diset
# standby_mode = 'on'
```

> :warning: **Perhatian**:
>
> - Pastikan menggunakan user `postgres`
> - Pastikan path direktori archive sesuai dengan yang sudah kita buat di masing-masing node ([lihat step pembuatan direktori archive](#membuat-direktori-penampung-wal-files)).

#### Menjalankan prosedur base-backup

Dalam konteks databasae replication, kesamaan state data antara database utama dengan replica sudah menjadi sesuatu keharusan. Untuk itu kita perlu memastikannya sebelum fungsi stream replication berjalan. Sekarang kita akan meng-_copy_ seluruh data yang ada pada MCL ke RCL. Namun sebelum itu kita perlu memastikan beberapa hal:

Yang pertama, pastikan kita me-_restart_ MCL agar konfigurasi yang sudah kita buat sebelumnya di-_reload_. Jalankan command berikut ini di MDE (:warning: jangan gunakan user `postgres`, gunakan user yang memiliki privilege `sudo`):

```bash
# Lakukan di MDE
sudo systemctl restart postgresql@12-main
```

Yang kedua, kita akan menjalankan prosedur base-backup di RDE. Prosedur base-backup ini harus dijalankan pada saat RCL dalam keadaan tidak aktif. Oleh karena itu pastikan kita menonaltifkan RCL terlebih dahulu (:warning: jangan gunakan user `postgres`, gunakan user yang memiliki privilege `sudo`)

```bash
# Lakukan di RDE
sudo systemctl stop postgresql@12-replica
```

Yang ketiga, setelah memastikan RCL tidak aktif kita harus menghapus seluruh data exisiting yang terdapat di RCL karena nantinya data di RCL akan kita isi dengan data yang kita copy dari MCL (:warning: pastikan menggunakan user `postgres`):

```bash
# Lakukan di RDE
cd /var/lib/postgresql/12/replica
rm -vRf *

# (Optional) pastikan kembali direktori sudah kosong
# ls -la
```

Sekarang kita sudah siap menjalankan prosedur basebackup. Jalankan command berikut ini di RDE (:warning: pastikan menggunakan user `postgres`):

```bash
pg_basebackup -h 10.10.1.10 -p 5432 -U replicator -D /var/lib/postgresql/12/replica -S replslot -w -P -R -Xs
```

| **Option** | **Keterangan**                                                                                                                   |
| :---       | :---                                                                                                                             |
| `-h`       | Host atau alamat IP dari server database utama yang hendak di-_copy_ datanya.                                                    |
| `-p`       | Nomor port cluster di server database utama.                                                                                     |
| `-U`       | Username yang digunakan untuk connect ke MCL                                                                                     |
| `-D`       | Path ke direktori tujuan dimana data yang di-_copy_ dari database utama hendak disimpan                                          |
| `-S`       | Nama replication slot yang hendak digunakan. Lihat [step membuat replication slot](#membuat-replication-slot).                   |
| `-w`       | Opsi agar prompt permintaan password tidak dimunculkan (karena user `replicator` yang kita buat tidak menggunakan password)      |
| `-P`       | Opsi agar menampilkan indikator progress (optional)                                                                              |
| `-R`       | Opsi yang menginstruksikan PG untuk membuat beberapa file penting untuk menjalankan proses replikasi _(akan kita bahas dibawah)_ |
| `-Xs`      | Opsi yang menginstruksikan PG untuk mengambil juga WAL files dengan metode `stream`

> :bulb: Gunakan command `man pg_basebackup` untuk melihat detail cara penggunaan command tersebut.

Apa yang terjadi ketika command `pb_basebackup` diatas dijalankan? Berikut ini sekilas aktifitasnya:

1. PG di RDE akan berusaha terhubung dengan PG di MDE menggunakan credential yang kita input pada option `-h, -p, -U,` dan `-w`.
2. Setelah terhubung, data di MCL keseluruhannya akan di-_copy_ kedalam directori di RCL yang path-nya kita sebutkan di option `-D`. Untuk memastikannya kita bisa periksa kembali isi dari direktori data RCL yang tadi sudah kita kosongkan (`/var/lib/postgresql/12/replica`). Idealnya direktori tersebut akan terisi oleh data yang di-_copy_ dari MCL.
3. Selain itu WAL files juga akan diambil dengan metode `stream`. Perilaku ini diakibatkan oleh option `-Xs` yang kita gunakan.
4. PG juga akan menulis beberapa file penting untuk menjalankan proses replication. File-file ini antara lain adalah `postgresql.auto.conf` dan `standby.signal` yang keduanya tersimpan berdampingan dengan data cluster di dalam `/var/lib/postgresql/12/replica`. File `postgresql.auto.conf` adalah file yang berisi pengaturan yang nantinya akan digunakan oleh PG di RCL untuk bisa terhubung dengan MCL. File ini tidak disarankan untuk diubal-ubah secara manual. Sedangkan file `standby.signal` adalah sebuah file kosong yang nantinya (saat RCL diaktifkan kembali) digunakan oleh PG sebagai penanda bahwa cluster harus berjalan dalam mode standby, dan pada mode standy operasi yang diizinkan hanya `SELECT` (read-only). File ini nantinya akan dihapus secara otomatis oleh RCL setelah digunakan.

#### Mengkatifkan Streaming Replication

Sampailah kita dilangkah terakhir pembuatan streaming replication pada PG yaitu mengaktifkan kembali RCL agar stream replication bisa berjalan. Silahkan eksekusi command berikut ini (:warning: jangan gunakan user `postgres`, gunakan user yang memiliki privilege `sudo`):

```bash
# Lakukan di RDE
sudo systemctl start postgresql@12-replica

# Periksa log RCL (optional) 
# sudo tail -n 100 /var/log/postgresql/postgresql-12-replica.log
```

## Tahap Pengujian

Setelah tahap pembuatan streaming replication kita lakukan, ada baiknya kita melakukan pengujian terhadap hasil streaming replication.

### Periksa status replikasi dengan command `pg_lsclusters`

Jalankan command `pg_lsclusters` di MDE dan RDE, kemudian periksa value yang tertulis pada kolom `Status`. Jika streaming replication berjalan lancar maka value yang tertulis untuk MCL idelanya adalah`online` sedangkan value yang tertulis di RCL idealnya adalah `online,recovery`.

### Periksa status replikasi dengan query

Jika RCL berjalan mulus dalam standby mode setelah diaktifkan kembali, maka idealnya RCL akan terkoneksi dengan MCL. Untuk membuktikannya kita bisa menjalankan command berikut ini di MCL (:warning: pastikan menggunakan user `postgres`):

```bash
psql -c "SELECT client_addr, state FROM pg_stat_replication;"

# The ideal output should be like:
#
#  client_addr    |  state
# ----------------+-----------
# your_replica_IP | streaming
```

Jika streaming replication berjalan lancar maka command diatas idealnya akan menampilkan table dengan 2 kolom dimana kolom pertama (`client_addr`) berisi alamat IP dari RCL dan kolom kedua (`state`) berisi status `streaming`.

### Validasi replikasi dengan operasi tulis

Selain dengan query ke tabel `pg_stat_replication`, kita juga sebaiknya melakukan pengujian dengan operasi tulis.

#### Operasi tulis dari MCL

Idealnya untuk setiap operasi tulis yang terjadi di MCL, RCL akan mereplikanya 100%. Untuk membuktikannya kita bisa uji dengan membuat sebuat database baru di MCL (:warning: pastikan menggunakan user `postgres`):

```bash
# Lakukan di MDE
psql -c "CREATE DATABASE db_test;"
```

Command diatas akan mengakibatkan PG di MCL menciptakan sebuah database baru bernama `db_test` yang idealnya juga akan tercipta di RCL. Oleh karena itu kita akan periksa kebenarannya di RCL (:warning: pastikan menggunakan user `postgres`):

```bash
# Lakukan di RDE
psql -c "\l"

# The output shoud be like:
#
#                              List of databases
#    Name    |  Owner   | Encoding | Collate |  Ctype  |   Access privileges   
# -----------+----------+----------+---------+---------+-----------------------
#  db_test   | postgres | UTF8     | C.UTF-8 | C.UTF-8 | 
#  postgres  | postgres | UTF8     | C.UTF-8 | C.UTF-8 | 
#  template0 | postgres | UTF8     | C.UTF-8 | C.UTF-8 | =c/postgres          +
#            |          |          |         |         | postgres=CTc/postgres
#  template1 | postgres | UTF8     | C.UTF-8 | C.UTF-8 | =c/postgres          +
#            |          |          |         |         | postgres=CTc/postgres
# (4 rows)
```

Jika pada kolom pertama dari tabel yang muncul terdapat `db_test`, maka bisa dikatakan streaming replication berjalan sebagaimana mestinya.

#### Operasi tulis di RCL

Telah kita bahas sebelumnya bahwa ketika suatu cluster PG berjalan sebagai replika (mode standby) maka operasi yang diizinkan hanyalah operasi baca (read-only). Untuk membuktikannya kita bisa melakukan pengujian dengan mencoba menjalankan operasi tulis pada RCL (:warning: pastikan menggunakan user `postgres`):

```bash
# Lakukan di RDE
psql -c "DROP DATABASE db_test;"

# The output shoud be:
#
# ERROR:  cannot execute DROP DATABASE in a read-only transaction
```

Jika command diatas dieksekusi di RDE, idealnya PG akan menampilkan pesan error `ERROR:  cannot execute DROP DATABASE in a read-only transaction`. Ini artinya operasi tulis tidak diizinkan dan bisa dikatakan streaming replication berjalan sebagaimana mestinya.

### Simuasi failure pada MDE dan solusi failover

Pengujian terakhir yang dapat kita lakukan adalah dengan meng-simulasikan kegagalan pada MDE dimana pada situasi ini RDE dapat mengambil peran sebagai node database utama menggantikan MDE.

#### Simulasikan kegagalan MDE

Cara paling sederhana untuk mensimulasikan kegagalan MDE adalah dengan menonaktifkan MCL (:warning: jangan gunakan user `postgres`, gunakan user yang memiliki privilege `sudo`):

```bash
# Lakukan di MDE
sudo systemctl stop postgresql@12-main
```

#### Promosikan RCL sebagai node DB utama pengganti MCL

Telah kita singgung sebelumnya bahwa salah satu benefit dari streaming replication adalah sebagai solusi failover ketika node database utama (MCL) mengalami kegagalan. Pada situasi tersebut RCL dapat mengambil peran menggantikan MCL sebagai database utama. Untuk melakukannya kita akan menggunakan command `pg_ctlcluster` di RCL:

```bash
# Lakukan di RCL
pg_ctlcluster 12 replica promote
```

> :bulb:**Tips**:
>
> - Pastikan menggunakan user `postgres`
> - Pantau keberhasilannya pada log RCL dengan mengeksekusi command `tail -n 100 /var/log/postgresql/postgresql-12-replica.log`.

#### Uji operasi tulis pada RCL (setelah dipromosikan)

Setelah RCL dipromosikan menjadi node DB utama pengganti MCL, idealnya RCL tidak lagi hanya menerima operasi baca melainkan juga operasi tulis. Untuk memastikannya lakukan operasi tulis pada RCL seperti contoh berikut ini (:warning: pastikan menggunakan user `postgres`):

```bash
# Lakukan di RDE
psql -c "DROP DATABASE db_test;"
```

Jika output dari command diatas adalah `DROP DATABASE`, maka operasi tulis pada RCL telah diizinkan dan ini artinya RCL telah sukses menggantikan MCL sebagai DB utama.

## :tada: Selamat

Sampai disini kita sudah berhasil membangun solusi failover untuk database dengan memanfaatkan fitur streaming replication pada PG. **Good Job !!!**

## Referensi

- [x] Catatan pribadi Pak Breman
- [x] [scalingpostgres.com - PostgreSQL Streaming Replication](https://www.scalingpostgres.com/tutorials/postgresql-streaming-replication/)
- [x] [scalingpostgres.com - PostgreSQL Replication Slots](https://www.scalingpostgres.com/tutorials/postgresql-replication-slots/)
- [x] [hevodata.com - PostgreSQL Replication Slots](https://hevodata.com/learn/postgresql-replication-slots/)
- [x] [DigitalOcean - How to Setup Physical Streaming Replication with PostgreSQL 12 on Ubuntu 20.04](https://www.digitalocean.com/community/tutorials/how-to-set-up-physical-streaming-replication-with-postgresql-12-on-ubuntu-20-04)
- [x] [PostgreSQL 12.10 Documentation](https://www.postgresql.org/docs/12/index.html)
