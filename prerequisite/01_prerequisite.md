# Scaling PostgreSQL - Prerequisite

[[_TOC_]]

Pada tutorial ini akan dibahas prasayat minimal yang dibutuhkan untuk scaling postgre.

## Kebutuhan Server

Berikut ini adalah daftar kebutuhan minimal dari server-server yang akan kita gunakan dalam praktik scaling PostgreSQL

- **2 server**, masing-masing dengan **OS Ubuntu 20.04**
- Server-server **berada pada jaringan yang sama**
- **Server pertama** akan kita gunakan sebagai **server database utama**
- **Server kedua** akan kita gunakan sebagai **server database replica**

> :warning: **Perhatian**:
>
> Untuk mempersingkat tulisan, sampai disini kita akan menyepakati beberapa hal kedepanya:
>
> - Server pertama _(main node)_ akan kita ganti istilahnya menjadi **MDE**
> - Server kedua _(replica node)_ akan kita ganti istilahnya menjadi **RDE**
> - MDE kita asumsikan memiliki IP **10.10.10.10**
> - RDE kita asumsikan memiliki IP **10.10.10.20**
> - Istilah untuk PostgreSQL akan kita ganti dengan **PG**
> - Istilah database akan kita ganti menjadi **DB**

## Kebutuhan Software

Setelah memastikan requirement server, kita akan memastikan kebutuhan software. Berikut ini adalah daftar software (diluar Ubuntu) yang akan kita gunakan sepanjang seri tutorial ini:

- [PostgreSQL versi 12](./02_setup-postgresql.md).
- [NFS](./03_setup-nfs.md) (akan kita gunakan pada tutorial WAL archiving).
- [Supervisor](./04_setup-supervisor.md) (akan kita gunakan pada tutorial WAL archiving).

> :bulb: klik masing-masing item diatas untuk menuju ke tutorial setup

## Konfigurasi dasar PG untuk Backup dan Replication

Setelah kebutuhan server dan kebutuhan software terpenuhi, lanjutkan dengan step [Setup Pre-Replication Configuration](./05_prereplication-config.md)
