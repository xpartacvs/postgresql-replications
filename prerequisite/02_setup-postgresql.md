# Scaling PostgreSQL - Setup PostgreSQL

[[_TOC_]]

## Install PostgreSQL 12

Software utama yang akan kita gunakan tentunya adalah PG. Dan versi PG yang akan kita gunakan pada seri tutorial ini adalah versi 12. Berikut ini adalah petunjuk instalasi dan pengaturannya pada masing-masing server.

Buka terminal dan eksekusi beberapa perintah berikut ini (lakukan di MDE & RDE):

```bash
# Tambahkan repository PG ke source list Ubuntu
sudo echo "deb http://apt.postgresql.org/pub/repos/apt $(lsb_release -cs)-pgdg main" > /etc/apt/sources.list.d/pgdg.list

# Tambahka signature key untuk repository postgre
wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -

# Update APT
sudo apt update

# Install PostgreSQL 12
sudo apt -y install postgresql-12
```

## Periksa hasil instalasi PG

Setelah berhasil ter-_install_ idealnya service PG akan langsung berjalan di Ubuntu. Beberapa hal yang direkomendasikan untuk dilakukan setelah proses instalasi PG adalah:

### Periksa status service PG pada systemd

Jalankan command berikut ini untuk memeriksa status service PG ada systemd (lakukan di MDE & RDE):

```bash
sudo systemctl status postgresql@12-main
```

Pada kondisi ideal, systemd akan menginformasikan bahwa service PG dalam keadaan aktif.

### Periksa kesiapan cluster PG

Saat PG pertama kali diinstal pada Ubuntu, biasanya PG akan membuat sebuat cluster default bernama **main** dan langsung akan menjalankannya. **Cluster** pada PG bisa diibaratkan sebagai suatu background service database independent (satu sama lain) yang berjalan diatas port tertentu _(default 5432)_ yang melayani permintaan operasional database.

#### Melihat daftar cluster

Untuk dapat melihat daftar cluster yang ada (berikut statusnya) pada suatu node, kita dapat menjalankan perintah berikut ini di terminal/console _(silahkan coba dimasing-masing node)_:

```bash
sudo pg_lsclusters
```

Contoh output:

```bash
Ver   Cluster Port    Status  Owner       Data directory              Log file
12    main    5432    online  postgress   /var/lib/postgresql/12/main /var/log/postgresql/postgresql-12-main.log
```

| **Kolom**      | **Keterangan**                                               |
| :---           | :---                                                         |
| Ver            | Versi dari PG yang mengelola cluster                         |
| Cluster        | Nama cluster                                                 |
| Port           | Nomor port yang digunkan oleh cluster untuk menerima request |
| Status         | Status cluster                                               |
| Owner          | User yang memiliki dan memicu jalannya cluster               |
| Data directory | Path ke direktori tempat data-data cluster tersimpan         |
| Log file       | Path ke file log dari cluster                                |

#### Membuat cluster baru

Jika ternyata cluster tidak ada, kita bisa membuat cluster baru dengan menggunakan command `pg_createcluster` seperti contoh dibawah ini:

```bash
sudo pg_createcluster 12 main --start
```

Jika command diatas dijalankan, maka PG akan membuat sebuah cluster versi `12` baru benama `main` sekaligus langsung menjalankannya (efek dari option `--start`).

> **Tips:**
>
> - Gunakan command `man pg_createcluster`untuk melihat panduan penggunaan command tersebut.
> - Untuk memastikan keadaan cluster, periksa kembali cluster yang sudah dibuat dengan command `sudo pg_lsclusters`

#### Mengubah nama cluster

Sampai disini kita sudah belajar cara melihat daftar cluster dengan command `pg_lsclusters` dan membuat cluster baru dengan command `pg_createcluster`. Selanjutnya kita akan belajar cara mengubah nama cluster. Wait..! Untuk apa?

Jika kita menjalankan perintah `pg_lsclusters` di masing-masing node (MDE dan RDE), maka akan kita dapati bahwa kedua cluster yang ada pada masing-masing node memiliki nama yang sama yaitu `main`. Oleh karena itu untuk menghidari kerancuan kedepannya, kita akan ubah nama cluster yang berada pada RDE (agar lebih sesuai dengan peruntukannya sebagai replika) menjadi `replica`.

Command yang akan kita gunakan untuk mengubah nama cluster adalah `pg_renamecluster`. Sekarang jalankan terminal command berikut ini pada RDE:

```bash
sudo pg_renamecluster 12 main replica
```

> **Tips**:
>
> - Gunakan command `man pg_renamecluster` untuk melihat panduan penggunaan command tersebut.

Setelah command diatas kita jalankan pada RDE, maka PG akan merubah nama cluster dari `main` menjadi `replica` (dengan terlebih dahulu menghentikan background processnya baru kemudian dijalankan lagi setelah nama cluster berubah).

Dan tidak hanya itu, PG juga merubah path ke diretori data cluster, konfigurasi dan juga path ke log cluster. Hal ini dapat kita buktikan dengan menampilkan ulang list cluster menggunakan command `pg_lsclusters`:

| **Kolom**        | **Sebelum `pg_renamecluster`**               | **Sesudah `pg_renamecluster`**                  |
| :---             | :---                                         | :---                                            |
| Cluster          | `main`                                       | `replica`                                       |
| Data Directory   | `/var/lib/postgresql/12/main`                | `/var/lib/postgresql/12/replica`                |
| Config Directory | `/etc/postgresql/12/main`                    | `/etc/postgresql/12/replica`                    |
| Log file         | `/var/log/postgresql/postgresql-12-main.log` | `/var/log/postgresql/postgresql-12-replica.log` |

> :warning: **Perhatian**:
>
> Sampai disini kita akan menyepakati beberapa asumsi tambahan yaitu:
>
> - Istilah `main` yang merujuk pada cluster di MDE untuk seterusnya akan kita ganti menjadi **MCL**.
> - Istilah `replica` yang merujuk pada cluster di RDE untuk seterusnya akan kita ganti menjadi **RCL**.

#### Menghapus cluster

Ada kalanya kita berada disituasi dimana kita diharuskan menghapus suatu cluster di PG. Part ini mungkin tidak berkaitan langsung dengan kebutuhan kita terhadap seri tutorial ini kedepannya, melainkan hanya sebagai pelengkap dari 3 operasi cluster yang sudah kita pelajari diatas.

Unutk dapat melakukan operasi penghapusan cluster di PG, kita dapat menggunakan command `pg_dropcluster` seperti contoh dibawah ini:

```bash
sudo pg_dropcluster --stop 12 broken-cluster
```

Jika command diatas dijalankan, maka PG akan menghapus cluster versi 12 bernama `broken-cluster` dengan terlebih dahulu menghentikan background process-nya (efek option `--stop`).

> **Tips**:
>
> - Gunakan command `man pg_dropcluster` untuk melihat detail cara penggunaan command tersebut.
> - Untuk memastikan keadaan cluster, periksa kembali dengan command `sudo pg_lsclusters`

### Periksa ketersediaan dedicated user PG

Selain cluster, saat pertama kali diinstal di Ubuntu idealnya PG juga akan membuat sebuah dedicated user dan group (dilevel OS) bernama `postgres`. User ini idealnya adalah user yang sebaiknya kita gunakan untuk operasi-operasi bersifat administratif di lingkup PG.

Cara paling tepat untuk memeriksa ketersedian user `postgres` ialah dengan _switch-as_ user tersebut menggunakan perintah `su` seperti contoh berikut ini:

```bash
sudo su - postgres
```

Jika tidak muncul pesan error, artinya kita sudah berhasil switch-as user `postgres`. Untuk memastikannya kita bisa jalankan command `whoami` (yang mana command ini akan mencetak username yang saat itu sedang kita gunakan). Sebaliknya jika muncul pesan error `su: user postgres does not exist` artinya user tersebut tidak ada atau gagal dibuat saat proses instalasi PG.

#### Periksa home directory milik user `postgres`

Setelah memastikan ketersediaan user `postgres` selanjutnya kita perlu memastikan ketersedian dan kepemilikan home directory dari user `postgres`. Pada ubuntu, home directory default dari user `postgres` adalah `/var/lib/postgresql`. Untuk memastikan kebenarannya, kita bisa memanfaatkan command `echo` untuk mencetak home direktori. Contoh `echo ~` atau `echo $HOME`. Jika yang tercetak pada terminal adalah `/var/lib/postgresql`maka home ditectory sudah benar (secara default).
