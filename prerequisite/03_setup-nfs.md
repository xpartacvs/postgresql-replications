# Scaling PostgreSQL - Setup NFS

[[_TOC_]]

_Network File System_ atau NFS akan kita gunakan untuk mengakomodir kebutuhan transfer file antar nodes (MDE & RDE) khususnya dalam scope backup & replication pada PG.

## Setup NFS Server di RDE

Kita akan jadikan RDE sebagai server utama dari NFS. Berikut ini langkah-langkahnya:

### Buat shared directory utama NFS

Sebelum menginstall NFS server, kita perlu mempersiapkan direktori yang akan menjadi shared directory utama untuk masing-masing node. Direktori utama ini nantinya akan di-_mount_ oleh NFS client yang berada pada node selain RDE.

Buka terminal/console RDE, dan eksekusi beberapa perintah berikut ini:

```bash
# Buat direktori seperti biasa
sudo mkdir -vp /share

# Ubah level permission direktori utama NFS
sudo chmod -vRf 777 /share

# Ubah kepemilikan direktori sesuai standard NFS
sudo chown -vRf nobody:nogroup /share
```

### Install NFS server

Buka terminal/console RDE, dan eksekusi beberapa perintah berikut ini:

```bash
# Update APT
sudo apt update

# Install server NFS
sudo apt -y install nfs-kernel-server
```

### Konfigurasi server NFS

Sekarang kita lakukan konfigurasi agar direktori utama yang sudah kita perisiapkan sebeumnya dijadikan sebagai shared directory utama NFS.

Buka file `/etc/exports` dengan text editor (misal: `sudo vim /etc/exports`) dan tambahkan baris script dibawah ini _(jangan lupa simpan setelahnya)_:

```txt
/share  10.10.10.20/24(rw,sync,no_root_squash,no_subtree_check)
```

Setelah itu eksekusi beberapa command berikut ini:

```bash
# Restart service NFS
sudo systemctl restart nfs-kernel-server

# Propagate directory NFS
sudo exportfs -va
```

## Setup NFS Client di MDE

Sekarang kita akan setup NFS client di MDE.

### Install NFS client

Buka terminal/console MDE, dan eksekusi beberapa perintah berikut ini:

```bash
# Update APT
sudo apt update

# Install server NFS
sudo apt -y install nfs-common
```

### Buat shared directory NFS

Seperti halnya NFS server, kita perlu mempersiapkan direktori yang akan dijadikan _mount-point_ untuk shared directory utama NFS.

Buka terminal/console MDE, dan eksekusi beberapa perintah berikut ini:

```bash
# Buat direktori seperti biasa
sudo mkdir -vp /mnt/nfs/share

# Ubah level permission direktori
sudo chmod -vRf 777 /mnt/nfs/share

# Ubah kepemilikan direktori sesuai standard NFS
sudo chown -vRf nobody:nogroup /mnt/nfs/share
```

### Konfigurasi mounting ubuntu via `fstab`

Selanjutnya kita perlu mengkonfigurasi `fstab` ubuntu agar selalu me-_mount_ shared directory NFS yang ada di RDE setiap kali MDE berjalan.

Edit file `/etc/fstab` (misal: `sudo vim /etc/fstab`) dan tambahkan baris script dibawah ini _(jangan lupa simpan setelahnya)_:

```txt
10.10.10.20:/share  /mnt/nfs/share  nfs4    auto,noatime,nolock,bg,nfsvers=4,intr,tcp,actimeo=1800  0   0
```

Kemudian restart MDE dengan command `sudo reboot`, dan periksa apakah shared directory sudah berhasil ter-_mount_ dengan command `sudo df -h`.

Output dari command `sudo df -h` adalah berupa daftar mount point (dalam format tabel) yang berjalan di Ubuntu. Dan diantara daftar tersebut, idealnya akan terdapat sebuah baris bertulisakan `10.10.10.20:/share` pada kolom paling kiri. Ini menandakan shared directory NFS sudah berhasil ter-_mount_ dengan benar.
