# Scaling PostgreSQL - Setup Supervisor

[[_TOC_]]

Supervisor biasanya digunakan sebagai tool bantuan untuk menjalankan suatu aplikasi agar aplikasi tersebut berjalan sebagai background perocess di linux.

Dalam konteks backup dan replication pada PG, salah satu tool bawaan PG adalah `pg_receivewal` dimana tool ini kita gunakan untuk melakukan archiving WAL files.

Secara default `pg_receivewal` hanya berjalan di-_foreground_, oleh karena itu kita membutuhkan supervisor agar `pg_receivewal` berjalan sebaga background process.

## Install Supervisor

Buka terminal linux dan eksekusi beberapa command berikut ini:

```bash
# Update APT
sudo apt update

# Install PostgreSQL 12
sudo apt -y install supervisor
```
