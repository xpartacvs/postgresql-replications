# Scaling PostgreSQL - Setup Pre-Replication Configuration

[[_TOC_]]

Dalam konteks perfoming backup & replication pada PG, kita perlu menyiapkan beberapa pengaturan tambahan:

## Membuat Role Replication

Untuk backup & replication, PG mengharuskan kita menggunakan role yang memiliki hak replication. Sekarang kita akan membuat role tersebut di MDE (gunakan user `postgres`):

```bash
# Eksekusi di MDE
psql -c "CREATE ROLE replicator WITH REPLICATION LOGIN;"
```

> :warning: **Perhatian**
>
> - Gunakan user `postgres` (eksekusi `sudo su - postgres` untuk menggunakan user `postgres`).
> - Kita sepakati role replication yang kita buat bernama `replicator`.
> - User `replicator` kita buat **tanpa password**

## Membuka akses koneksi PG antar node

Secara default PG di ubuntu tidak membuka akses network. Atau lebih tepatnya akses ke database hanya bisa dilakukan dari dalam host tempat PG terinstall. Sedangkan dalam konteks backup & replication kita membutuhkan (setidaknya) 2 node yang saling terhubung melalui network. Oleh karena itu kita perlu membuka akses network di kedua node yang kita gunakan (MDE & RDE).

Buatlah sebuah file baru (misal: `networks.conf`) di dalam direktori `conf.d` dimasing-masing node (pastikan melakukannya dengan menggunakan user `postgres`):

```bash
# Lakukan ini di MDE
touch /etc/postgresql/12/main/conf.d/networks.conf

# Lakukan ini di RDE
touch /etc/postgresql/12/replica/conf.d/networks.conf
```

Kemudian buka file `networks.conf` di masing-masing node dan tuliskan baris berikut ini didalamnya (jangan lupa simpan setelahnya) :

```ini
listen_addresses = '*'
```

## Mengkonfigurasi Host-based Authentication

Setelah membuka akses network dimasing-masing node, langkah selanjutnya adalah mendaftarkan user `replicator` pada Host-based Authentication (HBA).

Di Ubuntu HBA, diatur dalam sebuah file bernama `pg_hba.conf` yang berada didalam direktori dengan format path **_/etc/postgresql/\<versi-cluster\>/\<nama-cluster\>_**. Buka file tersebut dengan text edior seperti `vim`,`nano`, dan sejenisnya. Contoh: `vim /etc/postgresql/12/main` (lakukanlah di MDE dan di RDE). Didalam file `pg_hba.conf` kita akan melihat baris-baris teks yang tersesun menyerupai tabel dengan 5 kolom.

Tambahkan baris dibawah ini sebagai baris paling terakhir dari file tersebut (mengikuti pola yang ada), kemudian simpan dan tutup kembali file tersebut:

```bash
host    replication     replicator      10.10.10.1/24    trust
```

Dengan menuliskan baris diatas kita menginformasikan pada HBA bahwa akan ada satu atau lebih eksternal host  yang akan mengakses cluster untuk keperluan `replication` menggunakan user bernama `replicator` dari source IP antara `10.10.10.1 s/d 10.10.10.255` (CIDR `10.10.10.1/24`) yang harus diizinkan (`trust`).

> :bulb: Lihat [dokumentasi official HBA](https://www.postgresql.org/docs/12/auth-pg-hba-conf.html)

## Restart service PG di masing-masing node

Agar keseluruhan konfigurasi diatas segera diterapkan, restart service PG di masing-maing node (MDE & RDE):

```bash
# Eksekusi di MDE
sudo systemctl restart postgresql@12-main

# Eksekusi di RDE
sudo systemctl restart postgresql@12-replica
```
